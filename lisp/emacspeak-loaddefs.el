;;;Auto generated

;;;### (autoloads nil "amixer" "amixer.el" (0 0 0 0))
;;; Generated autoloads from amixer.el

(autoload 'amixer-restore "amixer" "\
Restore alsa settings.

\(fn &optional CONF-FILE)" nil nil)

(autoload 'amixer "amixer" "\
Manipulate ALSA settings.
Interactive prefix arg refreshes cache.

\(fn &optional REFRESH)" t nil)

(autoload 'amixer-store "amixer" "\
Persist current amixer settings." t nil)

(register-definition-prefixes "amixer" '("alsactl-program" "amixer-"))

;;;***

;;;### (autoloads nil "cd-tool" "cd-tool.el" (0 0 0 0))
;;; Generated autoloads from cd-tool.el

(autoload 'cd-tool "cd-tool" "\
Front-end to CDTool.
Bind this function to a convenient key-
Emacspeak users automatically have
this bound to <DEL> in the emacspeak keymap.

Key     Action
---     ------

+       Next Track
-       Previous Track
SPC     Pause or Resume
e       Eject
=       Shuffle
i       CD Info
p       Play
s       Stop
t       track
c       clip
cap C   Save clip to disk
" t nil)

(register-definition-prefixes "cd-tool" '("cd-tool-"))

;;;***

;;;### (autoloads nil "dectalk-voices" "dectalk-voices.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from dectalk-voices.el

(autoload 'dectalk-configure-tts "dectalk-voices" "\
Configures TTS environment to use Dectalk  synthesizers." nil nil)

(register-definition-prefixes "dectalk-voices" '("dectalk"))

;;;***

;;;### (autoloads nil "dom-addons" "dom-addons.el" (0 0 0 0))
;;; Generated autoloads from dom-addons.el

(register-definition-prefixes "dom-addons" '("dom-"))

;;;***

;;;### (autoloads nil "dtk-debug-tools" "dtk-debug-tools.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from dtk-debug-tools.el

(register-definition-prefixes "dtk-debug-tools" '("dtk--debug-speak-buffer"))

;;;***

;;;### (autoloads nil "dtk-interp" "dtk-interp.el" (0 0 0 0))
;;; Generated autoloads from dtk-interp.el

(register-definition-prefixes "dtk-interp" '("dtk-interp-" "tts-with-"))

;;;***

;;;### (autoloads nil "dtk-speak" "dtk-speak.el" (0 0 0 0))
;;; Generated autoloads from dtk-speak.el

(autoload 'ems-generate-switcher "dtk-speak" "\
Generate desired command to switch the specified state.

\(fn COMMAND SWITCH DOCUMENTATION)" nil nil)

(register-definition-prefixes "dtk-speak" '("dtk-" "ems-with-messages-silenced" "tts-"))

;;;***

;;;### (autoloads nil "dtk-unicode" "dtk-unicode.el" (0 0 0 0))
;;; Generated autoloads from dtk-unicode.el

(register-definition-prefixes "dtk-unicode" '("dtk-unicode-"))

;;;***

;;;### (autoloads nil "emacspeak" "emacspeak.el" (0 0 0 0))
;;; Generated autoloads from emacspeak.el

(defsubst emacspeak-setup-programming-mode nil "\
Setup programming mode. " (cl-declare (special dtk-split-caps emacspeak-audio-indentation)) (ems-with-messages-silenced (fset 'blink-matching-open (symbol-function 'emacspeak-blink-matching-open)) (dtk-set-punctuations 'all) (or dtk-split-caps (dtk-toggle-split-caps)) (emacspeak-pronounce-refresh-pronunciations) (or emacspeak-audio-indentation (emacspeak-toggle-audio-indentation))))

(autoload 'emacspeak "emacspeak" "\
Start the Emacspeak Audio Desktop.
Use Emacs as you normally would, emacspeak provides
 spoken feedback.  Emacspeak also provides commands
for having parts of the current buffer, the mode-line etc to be
spoken.

 Emacspeak commands use \\[emacspeak-prefix-command] as a prefix
key.  You can configure TTS  with
\\[emacspeak-dtk-submap-command] as a prefix.


\\{emacspeak-keymap}

Emacspeak provides a set of additional keymaps to give easy access to
its extensive facilities.

Press C-; to access keybindings in emacspeak-hyper-keymap:
\\{emacspeak-hyper-keymap}.

Press C-' or C-.  to access keybindings in emacspeak-super-keymap:
\\{emacspeak-super-keymap}.

Press C-, to access keybindings in emacspeak-alt-keymap:
\\{emacspeak-alt-keymap}.

See the online documentation \\[emacspeak-open-info] for individual
commands and options for details." nil nil)

(register-definition-prefixes "emacspeak" '("emacspeak-"))

;;;***

;;;### (autoloads nil "emacspeak-2048" "emacspeak-2048.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-2048.el

(register-definition-prefixes "emacspeak-2048" '("emacspeak-2048-"))

;;;***

;;;### (autoloads nil "emacspeak-actions" "emacspeak-actions.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-actions.el

(register-definition-prefixes "emacspeak-actions" '("emacspeak-"))

;;;***

;;;### (autoloads nil "emacspeak-advice" "emacspeak-advice.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-advice.el

(register-definition-prefixes "emacspeak-advice" '("emacspeak-" "ems-"))

;;;***

;;;### (autoloads nil "emacspeak-amark" "emacspeak-amark.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-amark.el

(autoload 'emacspeak-amark-save "emacspeak-amark" "\
Save buffer-local AMarks in  directory of currently playing resource." t nil)

(autoload 'emacspeak-amark-load "emacspeak-amark" "\
Locate AMarks file from `dir' current  directory is default, and load it.

\(fn &optional DIR)" nil nil)

(register-definition-prefixes "emacspeak-amark" '("emacspeak-amark-"))

;;;***

;;;### (autoloads nil "emacspeak-analog" "emacspeak-analog.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-analog.el

(register-definition-prefixes "emacspeak-analog" '("emacspeak-analog-"))

;;;***

;;;### (autoloads nil "emacspeak-ansi-color" "emacspeak-ansi-color.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-ansi-color.el

(register-definition-prefixes "emacspeak-ansi-color" '("emacspeak-ansi-color-to-voice"))

;;;***

;;;### (autoloads nil "emacspeak-arc" "emacspeak-arc.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-arc.el

(register-definition-prefixes "emacspeak-arc" '("emacspeak-arc"))

;;;***

;;;### (autoloads nil "emacspeak-auctex" "emacspeak-auctex.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-auctex.el

(register-definition-prefixes "emacspeak-auctex" '("emacspeak-auctex-"))

;;;***

;;;### (autoloads nil "emacspeak-autoload" "emacspeak-autoload.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-autoload.el

(register-definition-prefixes "emacspeak-autoload" '("emacspeak-auto-"))

;;;***

;;;### (autoloads nil "emacspeak-bbc" "emacspeak-bbc.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-bbc.el

(autoload 'emacspeak-bbc-schedule "emacspeak-bbc" "\
Browse BBC Schedule from get_iplayer radio cache" t nil)

(register-definition-prefixes "emacspeak-bbc" '("emacspeak-bbc-"))

;;;***

;;;### (autoloads nil "emacspeak-bookshare" "emacspeak-bookshare.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-bookshare.el

(autoload 'emacspeak-bookshare "emacspeak-bookshare" "\
Bookshare  Interaction." t nil)

(autoload 'emacspeak-bookshare-eww "emacspeak-bookshare" "\
Render complete book using EWW

\(fn DIRECTORY)" t nil)

(register-definition-prefixes "emacspeak-bookshare" '("emacspeak-bookshare-"))

;;;***

;;;### (autoloads nil "emacspeak-bs" "emacspeak-bs.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-bs.el

(register-definition-prefixes "emacspeak-bs" '("emacspeak-bs-speak-buffer-line"))

;;;***

;;;### (autoloads nil "emacspeak-buff-menu" "emacspeak-buff-menu.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-buff-menu.el

(register-definition-prefixes "emacspeak-buff-menu" '("emacspeak-list-buffers-"))

;;;***

;;;### (autoloads nil "emacspeak-c" "emacspeak-c.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-c.el

(register-definition-prefixes "emacspeak-c" '("c-" "emacspeak-c-s"))

;;;***

;;;### (autoloads nil "emacspeak-calculator" "emacspeak-calculator.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-calculator.el

(register-definition-prefixes "emacspeak-calculator" '("emacspeak-calculator-summarize"))

;;;***

;;;### (autoloads nil "emacspeak-calendar" "emacspeak-calendar.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-calendar.el

(autoload 'emacspeak-calendar-sunrise-sunset "emacspeak-calendar" "\
Display sunrise/sunset for specified address.

\(fn ADDRESS &optional ARG)" t nil)

(autoload 'emacspeak-calendar-setup-sunrise-sunset "emacspeak-calendar" "\
Set up geo-coordinates using Google Maps reverse geocoding.
To use, configure variable gmaps-my-address via M-x customize-variable." t nil)

(register-definition-prefixes "emacspeak-calendar" '("emacspeak-"))

;;;***

;;;### (autoloads nil "emacspeak-chess" "emacspeak-chess.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-chess.el

(register-definition-prefixes "emacspeak-chess" '("chess-" "emacspeak-chess-"))

;;;***

;;;### (autoloads nil "emacspeak-comint" "emacspeak-comint.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-comint.el

(autoload 'emacspeak-toggle-inaudible-or-comint-autospeak "emacspeak-comint" "\
Toggle comint-autospeak when in a comint or vterm buffer.
Otherwise call voice-setup-toggle-silence-personality which toggles the
personality under point." t nil)

(ems-generate-switcher 'emacspeak-toggle-comint-output-monitor 'emacspeak-comint-output-monitor "Toggle state of Emacspeak comint monitor.\nWhen turned on, comint output is automatically spoken.  Turn this on if\nyou want your shell to speak its results.  Interactive\nPREFIX arg means toggle the global default value, and then\nset the current local value to the result.")

(autoload 'emacspeak-comint-speech-setup "emacspeak-comint" "\
Speech setup for comint buffers." nil nil)

(register-definition-prefixes "emacspeak-comint" '("dirtrack-procfs-mode" "emacspeak-"))

;;;***

;;;### (autoloads nil "emacspeak-company" "emacspeak-company.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-company.el

(register-definition-prefixes "emacspeak-company" '("emacspeak-company-" "ems-company-current"))

;;;***

;;;### (autoloads nil "emacspeak-compile" "emacspeak-compile.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-compile.el

(register-definition-prefixes "emacspeak-compile" '("emacspeak-compilation-speak-error"))

;;;***

;;;### (autoloads nil "emacspeak-custom" "emacspeak-custom.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-custom.el

(register-definition-prefixes "emacspeak-custom" '("emacspeak-custom-"))

;;;***

;;;### (autoloads nil "emacspeak-dbus" "emacspeak-dbus.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-dbus.el

(autoload 'emacspeak-dbus-setup "emacspeak-dbus" "\
Turn on all defined handlers." nil nil)

(register-definition-prefixes "emacspeak-dbus" '("emacspeak-"))

;;;***

;;;### (autoloads nil "emacspeak-dired" "emacspeak-dired.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-dired.el

(autoload 'emacspeak-dired-downloads "emacspeak-dired" "\
Open Downloads directory." t nil)

(register-definition-prefixes "emacspeak-dired" '("emacspeak-"))

;;;***

;;;### (autoloads nil "emacspeak-dismal" "emacspeak-dismal.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-dismal.el

(register-definition-prefixes "emacspeak-dismal" '("emacspeak-dismal-"))

;;;***

;;;### (autoloads nil "emacspeak-ecb" "emacspeak-ecb.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ecb.el

(register-definition-prefixes "emacspeak-ecb" '("emacspeak-ecb-" "tree-node-is-expanded"))

;;;***

;;;### (autoloads nil "emacspeak-ediff" "emacspeak-ediff.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-ediff.el

(register-definition-prefixes "emacspeak-ediff" '("emacspeak-ediff-"))

;;;***

;;;### (autoloads nil "emacspeak-ein" "emacspeak-ein.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ein.el

(register-definition-prefixes "emacspeak-ein" '("emacspeak-ein-s"))

;;;***

;;;### (autoloads nil "emacspeak-elfeed" "emacspeak-elfeed.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-elfeed.el

(register-definition-prefixes "emacspeak-elfeed" '("emacspeak-elfeed-"))

;;;***

;;;### (autoloads nil "emacspeak-emms" "emacspeak-emms.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-emms.el

(autoload 'emacspeak-emms-pause-or-resume "emacspeak-emms" "\
Pause/resume if emms is running. For use  in
emacspeak-silence-hook." nil nil)

(register-definition-prefixes "emacspeak-emms" '("emacspeak-emms-speak-current-track"))

;;;***

;;;### (autoloads nil "emacspeak-enriched" "emacspeak-enriched.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-enriched.el

(register-definition-prefixes "emacspeak-enriched" '("emacspeak-enriched-"))

;;;***

;;;### (autoloads nil "emacspeak-entertain" "emacspeak-entertain.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-entertain.el

(register-definition-prefixes "emacspeak-entertain" '("emacspeak-hangman-s"))

;;;***

;;;### (autoloads nil "emacspeak-eperiodic" "emacspeak-eperiodic.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-eperiodic.el

(register-definition-prefixes "emacspeak-eperiodic" '("emacspeak-eperiodic-"))

;;;***

;;;### (autoloads nil "emacspeak-epub" "emacspeak-epub.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-epub.el

(autoload 'emacspeak-epub "emacspeak-epub" "\
EPub  Interaction.
When opened, displays a bookshelf consisting of  epubs found at the
root directory.
For detailed documentation, see \\[emacspeak-epub-mode]" t nil)

(register-definition-prefixes "emacspeak-epub" '("emacspeak-" "epub-t"))

;;;***

;;;### (autoloads nil "emacspeak-erc" "emacspeak-erc.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-erc.el

(register-definition-prefixes "emacspeak-erc" '("emacspeak-erc-"))

;;;***

;;;### (autoloads nil "emacspeak-eshell" "emacspeak-eshell.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-eshell.el

(register-definition-prefixes "emacspeak-eshell" '("emacspeak-eshell-"))

;;;***

;;;### (autoloads nil "emacspeak-etable" "emacspeak-etable.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-etable.el

(register-definition-prefixes "emacspeak-etable" '("emacspeak-etable-speak-cell"))

;;;***

;;;### (autoloads nil "emacspeak-eterm" "emacspeak-eterm.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-eterm.el

(register-definition-prefixes "emacspeak-eterm" '("emacspeak-eterm-" "eterm-"))

;;;***

;;;### (autoloads nil "emacspeak-eudc" "emacspeak-eudc.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-eudc.el

(register-definition-prefixes "emacspeak-eudc" '("emacspeak-eudc-"))

;;;***

;;;### (autoloads nil "emacspeak-evil" "emacspeak-evil.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-evil.el

(autoload 'emacspeak-evil-toggle-evil "emacspeak-evil" "\
Interactively toggle evil-mode." t nil)

(register-definition-prefixes "emacspeak-evil" '("emacspeak-evil-fix-emacspeak-prefix"))

;;;***

;;;### (autoloads nil "emacspeak-eww" "emacspeak-eww.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-eww.el

(defsubst emacspeak-eww-browser-check nil "\
Check  if  called from a EWW buffer" (cl-declare (special major-mode)) (unless (eq major-mode 'eww-mode) (error "This command cannot be used outside browser buffers.")))

(autoload 'emacspeak-eww-read-url "emacspeak-eww" "\
Return URL under point
or URL read from minibuffer." nil nil)

(autoload 'emacspeak-eww-autospeak "emacspeak-eww" "\
Setup post process hook to speak the Web page when rendered. " nil nil)

(defvar emacspeak-eww-pre-process-hook nil "\
Pre-process hook -- to be used for XSL preprocessing etc.")

(autoload 'emacspeak-eww-run-pre-process-hook "emacspeak-eww" "\
Run web pre process hook.

\(fn &rest IGNORE)" nil nil)

(defvar emacspeak-eww-post-process-hook nil "\
Set locally to a  site specific post processor.
Note that the Web browser should reset this hook after using it.")

(autoload 'emacspeak-eww-run-post-process-hook "emacspeak-eww" "\
Use web post process hook.

\(fn &rest IGNORE)" nil nil)

(autoload 'emacspeak-eww-open-mark "emacspeak-eww" "\
Open specified EWW marked location. If the content is already being
displayed in this Emacs session, jump to it directly. With optional
interactive prefix arg `delete', delete that mark instead.

\(fn NAME &optional DELETE)" t nil)

(autoload 'emacspeak-eww-smart-tabs "emacspeak-eww" "\
Open URL in EWW keyed by  `char'.
To associate a URL with a char, use this command
with an interactive prefix arg. 

\(fn CHAR &optional DEFINE)" t nil)

(register-definition-prefixes "emacspeak-eww" '("eww-" "shr-tag-math"))

;;;***

;;;### (autoloads nil "emacspeak-feeds" "emacspeak-feeds.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-feeds.el

(autoload 'emacspeak-feeds-rss-display "emacspeak-feeds" "\
Display RSS feed.

\(fn FEED-URL)" t nil)

(autoload 'emacspeak-feeds-opml-display "emacspeak-feeds" "\
Display OPML feed.

\(fn FEED-URL)" t nil)

(autoload 'emacspeak-feeds-atom-display "emacspeak-feeds" "\
Display ATOM feed.

\(fn FEED-URL)" t nil)

(autoload 'emacspeak-feeds-browse "emacspeak-feeds" "\
Browse specified  feed.

\(fn FEED)" t nil)

(register-definition-prefixes "emacspeak-feeds" '("emacspeak-feeds"))

;;;***

;;;### (autoloads nil "emacspeak-filtertext" "emacspeak-filtertext.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-filtertext.el

(autoload 'emacspeak-filtertext "emacspeak-filtertext" "\
Copy over text in region to special filtertext buffer in
preparation for interactively filtering text. 

\(fn START END)" t nil)

(register-definition-prefixes "emacspeak-filtertext" '("emacspeak-filtertext-"))

;;;***

;;;### (autoloads nil "emacspeak-fix-interactive" "emacspeak-fix-interactive.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-fix-interactive.el

(register-definition-prefixes "emacspeak-fix-interactive" '("emacspeak-" "ems-prompt-without-minibuffer-p"))

;;;***

;;;### (autoloads nil "emacspeak-flyspell" "emacspeak-flyspell.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-flyspell.el

(register-definition-prefixes "emacspeak-flyspell" '("emacspeak-flyspell-correct"))

;;;***

;;;### (autoloads nil "emacspeak-forms" "emacspeak-forms.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-forms.el

(autoload 'emacspeak-forms-find-file "emacspeak-forms" "\
Visit a forms file

\(fn FILENAME)" t nil)

(register-definition-prefixes "emacspeak-forms" '("emacspeak-forms-"))

;;;***

;;;### (autoloads nil "emacspeak-gh-explorer" "emacspeak-gh-explorer.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-gh-explorer.el

(register-definition-prefixes "emacspeak-gh-explorer" '("emacspeak-gh-explorer-" "ems--gh-explorer-nav"))

;;;***

;;;### (autoloads nil "emacspeak-gnus" "emacspeak-gnus.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-gnus.el

(register-definition-prefixes "emacspeak-gnus" '("emacspeak-gnus-" "gnus-summary-downcase-article"))

;;;***

;;;### (autoloads nil "emacspeak-gomoku" "emacspeak-gomoku.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-gomoku.el

(register-definition-prefixes "emacspeak-gomoku" '("emacspeak-gomoku-" "gomoku-"))

;;;***

;;;### (autoloads nil "emacspeak-google" "emacspeak-google.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-google.el

(autoload 'emacspeak-google-tts "emacspeak-google" "\
Speak text using Google Network TTS.
Optional interactive prefix arg `lang' specifies  language identifier.

\(fn TEXT &optional LANG)" t nil)

(autoload 'emacspeak-google-tts-region "emacspeak-google" "\
Speak region using Google Network TTS.

\(fn START END &optional ASK-LANG)" t nil)

(register-definition-prefixes "emacspeak-google" '("emacspeak-google-"))

;;;***

;;;### (autoloads nil "emacspeak-gridtext" "emacspeak-gridtext.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-gridtext.el

(define-prefix-command 'emacspeak-gridtext 'emacspeak-gridtext-keymap)

(register-definition-prefixes "emacspeak-gridtext" '("emacspeak-gridtext-"))

;;;***

;;;### (autoloads nil "emacspeak-helm" "emacspeak-helm.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-helm.el

(register-definition-prefixes "emacspeak-helm" '("emacspeak-helm-"))

;;;***

;;;### (autoloads nil "emacspeak-hide" "emacspeak-hide.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-hide.el

(autoload 'emacspeak-hide-all-blocks-in-buffer "emacspeak-hide" "\
Hide all blocks in current buffer." nil nil)

(autoload 'emacspeak-hide-or-expose-block "emacspeak-hide" "\
Hide or expose a block of text.
This command either hides or exposes a block of text
starting on the current line.  A block of text is defined as
a portion of the buffer in which all lines start with a
common PREFIX.  Optional interactive prefix arg causes all
blocks in current buffer to be hidden or exposed.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-hide-or-expose-all-blocks "emacspeak-hide" "\
Hide or expose all blocks in buffer." t nil)

(autoload 'emacspeak-hide-speak-block-sans-prefix "emacspeak-hide" "\
Speaks current block after stripping its prefix.
If the current block is not hidden, it first hides it.
This is useful because as you locate blocks, you can invoke this
command to listen to the block,
and when you have heard enough navigate easily  to move past the block." t nil)

(register-definition-prefixes "emacspeak-hide" '("emacspeak-hid"))

;;;***

;;;### (autoloads nil "emacspeak-hydra" "emacspeak-hydra.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-hydra.el

(register-definition-prefixes "emacspeak-hydra" '("emacspeak-hydra-" "ems--lv-cache"))

;;;***

;;;### (autoloads nil "emacspeak-ibuffer" "emacspeak-ibuffer.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-ibuffer.el

(register-definition-prefixes "emacspeak-ibuffer" '("emacspeak-ibuffer-s"))

;;;***

;;;### (autoloads nil "emacspeak-ido" "emacspeak-ido.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ido.el

(register-definition-prefixes "emacspeak-ido" '("emacspeak-ido-"))

;;;***

;;;### (autoloads nil "emacspeak-info" "emacspeak-info.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-info.el

(autoload 'emacspeak-info-wizard "emacspeak-info" "\
Read a node spec from the minibuffer and launch
Info-goto-node.
See documentation for command `Info-goto-node' for details on
node-spec.

\(fn NODE-SPEC)" t nil)

(register-definition-prefixes "emacspeak-info" '("emacspeak-info-"))

;;;***

;;;### (autoloads nil "emacspeak-ispell" "emacspeak-ispell.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-ispell.el

(register-definition-prefixes "emacspeak-ispell" '("emacspeak-ispell-max-choices"))

;;;***

;;;### (autoloads nil "emacspeak-ivy" "emacspeak-ivy.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ivy.el

(register-definition-prefixes "emacspeak-ivy" '("emacspeak-ivy-speak-selection"))

;;;***

;;;### (autoloads nil "emacspeak-jabber" "emacspeak-jabber.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-jabber.el

(register-definition-prefixes "emacspeak-jabber" '("emacspeak-jabber-"))

;;;***

;;;### (autoloads nil "emacspeak-js2" "emacspeak-js2.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-js2.el

(register-definition-prefixes "emacspeak-js2" '("emacspeak-js2-hook"))

;;;***

;;;### (autoloads nil "emacspeak-keymap" "emacspeak-keymap.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-keymap.el

(autoload 'ems-kbd "emacspeak-keymap" "\
Like function kbd, but returns a vector.

\(fn STRING)" nil nil)

(autoload 'emacspeak-keymap-update "emacspeak-keymap" "\
Update keymap with  binding.

\(fn KEYMAP BINDING)" nil nil)

(register-definition-prefixes "emacspeak-keymap" '("emacspeak-" "ems--kbd-"))

;;;***

;;;### (autoloads nil "emacspeak-librivox" "emacspeak-librivox.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-librivox.el

(autoload 'emacspeak-librivox "emacspeak-librivox" "\
Launch a Librivox Search.

\(fn SEARCH-TYPE)" t nil)

(register-definition-prefixes "emacspeak-librivox" '("emacspeak-librivox-"))

;;;***

;;;### (autoloads nil "emacspeak-lispy" "emacspeak-lispy.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-lispy.el

(register-definition-prefixes "emacspeak-lispy" '("emacspeak-lispy-setup"))

;;;***

;;;### (autoloads nil "emacspeak-load-path" "emacspeak-load-path.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-load-path.el

(defsubst ems-interactive-p nil "\
Check  interactive flag.
Return T if set and we are called from the advice for the current
 command. Turn off the flag once used." (when ems-called-interactively-p (let ((caller (cl-second (backtrace-frame 1))) (caller-advice (ad-get-advice-info-field ems-called-interactively-p 'advicefunname)) (result nil)) (setq result (eq caller caller-advice)) (when result (setq ems-called-interactively-p nil) result))))

(register-definition-prefixes "emacspeak-load-path" '("ems-"))

;;;***

;;;### (autoloads nil "emacspeak-m-player" "emacspeak-m-player.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-m-player.el

(defvar emacspeak-m-player-program (executable-find "mplayer") "\
Media player program.")

(custom-autoload 'emacspeak-m-player-program "emacspeak-m-player" t)

(defvar emacspeak-media-location-bindings nil "\
Map specific key sequences to launching MPlayer accelerators
on a specific directory.")

(custom-autoload 'emacspeak-media-location-bindings "emacspeak-m-player" nil)

(autoload 'emacspeak-multimedia "emacspeak-m-player" "\
Start or control Emacspeak multimedia player.

Uses current context to prompt for media to play.
Controls media playback when already playing a stream.

\\{emacspeak-m-player-mode-map}." t nil)

(autoload 'emacspeak-m-player-bind-accelerator "emacspeak-m-player" "\
Binds key to invoke m-player  on specified directory.

\(fn DIRECTORY KEY)" t nil)

(autoload 'emacspeak-m-player-url "emacspeak-m-player" "\
Call emacspeak-m-player with specified URL.

\(fn URL &optional PLAYLIST-P)" t nil)

(autoload 'emacspeak-m-player "emacspeak-m-player" "\
Play specified resource using m-player.  Optional prefix argument
play-list interprets resource as a play-list.  Second interactive
prefix arg adds option -allow-dangerous-playlist-parsing to mplayer.
Resource is a media resource or playlist containing media resources.
The player is placed in a buffer in emacspeak-m-player-mode.

\(fn RESOURCE &optional PLAY-LIST)" t nil)

(autoload 'emacspeak-m-player-using-openal "emacspeak-m-player" "\
Use openal as the audio output driver. Adding hrtf=true to
~/.alsoftrc gives HRTF. You need to have openal installed and have an
mplayer that has been compiled with openal support to use this
feature. Calling spec is like `emacspeak-m-player'.

\(fn RESOURCE &optional PLAY-LIST)" t nil)

(autoload 'emacspeak-m-player-shuffle "emacspeak-m-player" "\
Launch M-Player with shuffle turned on." t nil)

(defvar emacspeak-m-player-media-history nil "\
Record media urls we played.")

(autoload 'emacspeak-m-player-youtube-player "emacspeak-m-player" "\
Use youtube-dl and mplayer to stream the audio for YouTube content.
Default is to pick smallest (lowest quality) audio format.
Optional prefix arg `best' chooses highest quality.

\(fn URL &optional BEST)" t nil)

(autoload 'emacspeak-m-player-play-rss "emacspeak-m-player" "\
Play an RSS stream by converting to  an M3U playlist.

\(fn RSS-URL)" t nil)

(autoload 'emacspeak-m-player-locate-media "emacspeak-m-player" "\
Locate media matching specified pattern.  The results can be
played as a play-list by pressing [RET] on the first line.
Pattern is first converted to a regexp that accepts common
punctuation separators (-,._'\") in place of white-space.
Results are placed in a Locate buffer and can be played using
M-Player --- use \\[emacspeak-dired-open-this-file] locally bound to C-RET 
to play individual tracks.

\(fn PATTERN)" t nil)

(register-definition-prefixes "emacspeak-m-player" '("emacspeak-" "ems-"))

;;;***

;;;### (autoloads nil "emacspeak-magit" "emacspeak-magit.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-magit.el

(register-definition-prefixes "emacspeak-magit" '("emacspeak-magit-blame-speak"))

;;;***

;;;### (autoloads nil "emacspeak-man" "emacspeak-man.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-man.el

(register-definition-prefixes "emacspeak-man" '("emacspeak-man-"))

;;;***

;;;### (autoloads nil "emacspeak-maths" "emacspeak-maths.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-maths.el

(autoload 'emacspeak-maths-start "emacspeak-maths" "\
Start Maths server bridge." t nil)

(autoload 'emacspeak-maths-enter-guess "emacspeak-maths" "\
Send the guessed  LaTeX expression to Maths server.
Guess is based on context." t nil)

(autoload 'emacspeak-maths-enter "emacspeak-maths" "\
Send a LaTeX expression to Maths server.
Tries to guess default based on context. 

\(fn LATEX)" t nil)

(register-definition-prefixes "emacspeak-maths" '("emacspeak-maths"))

;;;***

;;;### (autoloads nil "emacspeak-message" "emacspeak-message.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-message.el

(register-definition-prefixes "emacspeak-message" '("emacspeak-message-punctuation-mode"))

;;;***

;;;### (autoloads nil "emacspeak-midge" "emacspeak-midge.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-midge.el

(register-definition-prefixes "emacspeak-midge" '("midge-mode-hook"))

;;;***

;;;### (autoloads nil "emacspeak-mines" "emacspeak-mines.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-mines.el

(register-definition-prefixes "emacspeak-mines" '("emacspeak-mines-"))

;;;***

;;;### (autoloads nil "emacspeak-mspools" "emacspeak-mspools.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-mspools.el

(register-definition-prefixes "emacspeak-mspools" '("mspools-"))

;;;***

;;;### (autoloads nil "emacspeak-muggles" "emacspeak-muggles.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-muggles.el

(autoload 'emacspeak-muggles-ido-yank "emacspeak-muggles" "\
Pick what to yank using ido completion." t nil)

(register-definition-prefixes "emacspeak-muggles" '("emacspeak-muggles-"))

;;;***

;;;### (autoloads nil "emacspeak-newsticker" "emacspeak-newsticker.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-newsticker.el

(register-definition-prefixes "emacspeak-newsticker" '("emacspeak-newsticker-summarize-item"))

;;;***

;;;### (autoloads nil "emacspeak-nov" "emacspeak-nov.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-nov.el

(register-definition-prefixes "emacspeak-nov" '("emacspeak-nov-mode-hook"))

;;;***

;;;### (autoloads nil "emacspeak-nxml" "emacspeak-nxml.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-nxml.el

(register-definition-prefixes "emacspeak-nxml" '("emacspeak-nxml-summarize-outline"))

;;;***

;;;### (autoloads nil "emacspeak-ocr" "emacspeak-ocr.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ocr.el

(autoload 'emacspeak-ocr-customize "emacspeak-ocr" "\
Customize OCR settings." t nil)

(autoload 'emacspeak-ocr "emacspeak-ocr" "\
An OCR front-end for the Emacspeak desktop.  

Page image is acquired using tools from the SANE package.
The acquired image is run through the OCR engine if one is
available, and the results placed in a buffer that is
suitable for browsing the results.

For detailed help, invoke command emacspeak-ocr bound to
\\[emacspeak-ocr] to launch emacspeak-ocr-mode, and press
`?' to display mode-specific help for emacspeak-ocr-mode." t nil)

(register-definition-prefixes "emacspeak-ocr" '("emacspeak-ocr-"))

;;;***

;;;### (autoloads nil "emacspeak-org" "emacspeak-org.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-org.el

(autoload 'emacspeak-org-popup-input "emacspeak-org" "\
Pops up an org input area." t nil)

(autoload 'emacspeak-org-popup-input-buffer "emacspeak-org" "\
Provide an input buffer in a specified mode.

\(fn MODE)" t nil)

(register-definition-prefixes "emacspeak-org" '("emacspeak-org-" "org-ans2" "tvr-org-"))

;;;***

;;;### (autoloads nil "emacspeak-origami" "emacspeak-origami.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-origami.el

(register-definition-prefixes "emacspeak-origami" '("emacspeak-origami-invisible-p"))

;;;***

;;;### (autoloads nil "emacspeak-outline" "emacspeak-outline.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-outline.el

(register-definition-prefixes "emacspeak-outline" '("emacspeak-outline-"))

;;;***

;;;### (autoloads nil "emacspeak-package" "emacspeak-package.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-package.el

(register-definition-prefixes "emacspeak-package" '("emacspeak-package-"))

;;;***

;;;### (autoloads nil "emacspeak-paradox" "emacspeak-paradox.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-paradox.el

(register-definition-prefixes "emacspeak-paradox" '("emacspeak-paradox-"))

;;;***

;;;### (autoloads nil "emacspeak-pianobar" "emacspeak-pianobar.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-pianobar.el

(autoload 'emacspeak-pianobar "emacspeak-pianobar" "\
Start or control Emacspeak Pianobar player." t nil)

(register-definition-prefixes "emacspeak-pianobar" '("emacspeak-pianobar-"))

;;;***

;;;### (autoloads nil "emacspeak-popup" "emacspeak-popup.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-popup.el

(register-definition-prefixes "emacspeak-popup" '("emacspeak-popup-speak-item"))

;;;***

;;;### (autoloads nil "emacspeak-proced" "emacspeak-proced.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-proced.el

(register-definition-prefixes "emacspeak-proced" '("emacspeak-proced-"))

;;;***

;;;### (autoloads nil "emacspeak-projectile" "emacspeak-projectile.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-projectile.el

(register-definition-prefixes "emacspeak-projectile" '("emacspeak-projectile-file-action"))

;;;***

;;;### (autoloads nil "emacspeak-pronounce" "emacspeak-pronounce.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-pronounce.el

(autoload 'emacspeak-pronounce-add-dictionary-entry "emacspeak-pronounce" "\
 Adds pronunciation pair STRING.PRONUNCIATION to the dictionary.
Argument KEY specifies a dictionary key e.g. directory, mode etc.
Pronunciation can be a string or a cons-pair.
If it is a string, that string is the new pronunciation.
A cons-pair of the form (matcher . func) results  in 
the match  being passed to the func which returns  the new pronunciation.

\(fn KEY STRING PRONUNCIATION)" nil nil)

(autoload 'emacspeak-pronounce-load-dictionaries "emacspeak-pronounce" "\
Load pronunciation dictionaries.
Optional argument FILENAME specifies the dictionary file,
Default is emacspeak-pronounce-dictionaries-file.

\(fn &optional FILENAME)" t nil)

(register-definition-prefixes "emacspeak-pronounce" '("emacspeak-pronounce-"))

;;;***

;;;### (autoloads nil "emacspeak-rmail" "emacspeak-rmail.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-rmail.el

(register-definition-prefixes "emacspeak-rmail" '("emacspeak-rmail-summarize-"))

;;;***

;;;### (autoloads nil "emacspeak-rpm-spec" "emacspeak-rpm-spec.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-rpm-spec.el

(register-definition-prefixes "emacspeak-rpm-spec" '("emacspeak-rpm-spec-"))

;;;***

;;;### (autoloads nil "emacspeak-rust-mode" "emacspeak-rust-mode.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-rust-mode.el

(register-definition-prefixes "emacspeak-rust-mode" '("emacspeak-rust-mode-setup"))

;;;***

;;;### (autoloads nil "emacspeak-sage" "emacspeak-sage.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-sage.el

(register-definition-prefixes "emacspeak-sage" '("emacspeak-sage-"))

;;;***

;;;### (autoloads nil "emacspeak-sdcv" "emacspeak-sdcv.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-sdcv.el

(register-definition-prefixes "emacspeak-sdcv" '("emacspeak-sdcv-"))

;;;***

;;;### (autoloads nil "emacspeak-ses" "emacspeak-ses.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ses.el

(register-definition-prefixes "emacspeak-ses" '("emacspeak-ses-"))

;;;***

;;;### (autoloads nil "emacspeak-setup" "emacspeak-setup.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-setup.el

(defvar emacspeak-directory (expand-file-name "../" (file-name-directory load-file-name)) "\
emacspeak installation directory")

(defvar emacspeak-lisp-directory (expand-file-name "lisp/" emacspeak-directory) "\
Emacspeak lisp directory.")

(defvar emacspeak-sounds-directory (expand-file-name "sounds/" emacspeak-directory) "\
Auditory icons directory.")

(defvar emacspeak-xslt-directory (expand-file-name "xsl/" emacspeak-directory) "\
XSL transformations.")

(defvar emacspeak-etc-directory (expand-file-name "etc/" emacspeak-directory) "\
Misc directory.")

(defvar emacspeak-servers-directory (expand-file-name "servers/" emacspeak-directory) "\
Speech servers directory.")

(defvar emacspeak-info-directory (expand-file-name "info/" emacspeak-directory) "\
Emacspeak reference.")

(defvar emacspeak-user-directory (expand-file-name "~/.emacspeak/") "\
Emacspeak resources, e.g. pronunciation dicts.")

(defvar emacspeak-readme-file (expand-file-name "README" emacspeak-directory) "\
README file with  Git  revision number.")

(defvar emacspeak-curl-program (executable-find "curl") "\
Curl location.")

(defvar emacspeak-media-extensions (eval-when-compile (let ((ext '("mov" "wma" "wmv" "flv" "m4a" "m4b" "flac" "aiff" "aac" "opus " "mkv" "ogv" "oga" "ogg" "mp3" "mp4" "webm" "wav"))) (concat "\\." (regexp-opt (nconc ext (mapcar #'upcase ext)) 'parens) "$"))) "\
Media file Extensions.")

(defvar emacspeak-m-player-playlist-pattern (eval-when-compile (concat (regexp-opt (list ".m3u" ".asx" ".pls" ".rpm" ".ram")) "$")) "\
Playlist pattern.")

;;;***

;;;### (autoloads nil "emacspeak-shx" "emacspeak-shx.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-shx.el

(register-definition-prefixes "emacspeak-shx" '("shx-cmd-"))

;;;***

;;;### (autoloads nil "emacspeak-solitaire" "emacspeak-solitaire.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-solitaire.el

(register-definition-prefixes "emacspeak-solitaire" '("emacspeak-solitaire-"))

;;;***

;;;### (autoloads nil "emacspeak-sounds" "emacspeak-sounds.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-sounds.el

(defsubst emacspeak-sounds-define-theme (theme-name file-ext) "\
Define a sounds theme for auditory icons. " (cl-declare (special emacspeak-sounds-themes-table)) (setq theme-name (intern theme-name)) (setf (gethash theme-name emacspeak-sounds-themes-table) file-ext))

(autoload 'emacspeak-serve-auditory-icon "emacspeak-sounds" "\
Serve auditory icon SOUND-NAME.

\(fn SOUND-NAME)" nil nil)

(defvar emacspeak-sox (executable-find "sox") "\
Name of SoX executable.")

(autoload 'emacspeak-auditory-icon "emacspeak-sounds" "\
Play an auditory ICON.

\(fn ICON)" nil nil)

(autoload 'emacspeak-prompt "emacspeak-sounds" "\
Play  prompt for specified name.

\(fn NAME)" nil nil)

(register-definition-prefixes "emacspeak-sounds" '("emacspeak-"))

;;;***

;;;### (autoloads nil "emacspeak-speak" "emacspeak-speak.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-speak.el

(autoload 'emacspeak-speak-run-shell-command "emacspeak-speak" "\
Invoke shell COMMAND and display its output as a table. The
results are placed in a buffer in Emacspeak's table browsing
mode. Optional interactive prefix arg read-as-csv interprets the
result as csv. . Use this for running shell commands that produce
tabulated output. This command should be used for shell commands
that produce tabulated output that works with Emacspeak's table
recognizer. Verify this first by running the command in a shell
and executing command `emacspeak-table-display-table-in-region'
normally bound to \\[emacspeak-table-display-table-in-region].

\(fn COMMAND &optional READ-AS-CSV)" t nil)

(ems-generate-switcher 'emacspeak-toggle-audio-indentation 'emacspeak-audio-indentation "Toggle state of  Emacspeak  audio indentation.\nInteractive PREFIX arg means toggle  the global default value, and then set the\ncurrent local  value to the result.\nSpecifying the method of indentation as `tones'\nresults in the Dectalk producing a tone whose length is a function of the\nline's indentation.  Specifying `speak'\nresults in the number of initial spaces being spoken.")

(defvar-local emacspeak-action-mode nil "\
Determines if action mode is active.
Non-nil value means that any function that is set as the
value of property action is executed when the text at that
point is spoken.")

(autoload 'emacspeak-handle-action-at-point "emacspeak-speak" "\
Execute action specified at point.

\(fn &optional POS)" nil nil)

(autoload 'emacspeak-speak-line "emacspeak-speak" "\
Speaks current line.  With prefix ARG, speaks the rest of the line
from point.  Negative prefix optional arg speaks from start of line to
point.  Voicifies if option `voice-lock-mode' is on.  Indicates
indentation with a tone or a spoken message if audio indentation is in
use see `emacspeak-toggle-audio-indentation' bound to
\\[emacspeak-toggle-audio-indentation].  Indicates position of point
with an aural highlight if option `emacspeak-show-point' is turned on
--see command `emacspeak-toggle-show-point' bound to
\\[emacspeak-toggle-show-point].  Lines that start hidden blocks of text,
e.g.  outline header lines, or header lines of blocks created by
command `emacspeak-hide-or-expose-block' are indicated with auditory
icon ellipses. Presence of additional presentational overlays (created
via property display, before-string, or after-string) is indicated
with auditory icon `more'.  These can then be spoken using command
\\[emacspeak-speak-overlay-properties].

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-visual-line "emacspeak-speak" "\
Speaks current visual line.
Cues the start of a physical line with auditory icon `left'." t nil)

(autoload 'emacspeak-speak-word "emacspeak-speak" "\
Speak current word.
With prefix ARG, speaks the rest of the word from point.
Negative prefix arg speaks from start of word to point.
If executed  on the same buffer position a second time, the word is
spelled out  instead of being spoken.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-char "emacspeak-speak" "\
Speak character under point.
Pronounces character phonetically unless  called with a PREFIX arg.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-speak-preceding-char "emacspeak-speak" "\
Speak character before point." t nil)

(autoload 'emacspeak-speak-char-name "emacspeak-speak" "\
tell me what this is

\(fn CHAR)" t nil)

(autoload 'emacspeak-speak-display-char "emacspeak-speak" "\
Display char under point using current speech display table.
Behavior is the same as command `emacspeak-speak-char'
bound to \\[emacspeak-speak-char]
for characters in the range 0--127.
Optional argument PREFIX  specifies that the character should be spoken phonetically.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-speak-sentence "emacspeak-speak" "\
Speak current sentence.
With prefix ARG, speaks the rest of the sentence  from point.
Negative prefix arg speaks from start of sentence to point.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-sexp "emacspeak-speak" "\
Speak current sexp.
With prefix ARG, speaks the rest of the sexp  from point.
Negative prefix arg speaks from start of sexp to point. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-page "emacspeak-speak" "\
Speak a page.
With prefix ARG, speaks rest of current page.
Negative prefix arg will read from start of current page to point.
If option  `voice-lock-mode' is on, then it will use any defined personality.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-paragraph "emacspeak-speak" "\
Speak paragraph.
With prefix arg, speaks rest of current paragraph.
Negative prefix arg will read from start of current paragraph to point.
If voice-lock-mode is on, then it will use any defined personality. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-buffer "emacspeak-speak" "\
Speak current buffer  contents.
With prefix ARG, speaks the rest of the buffer from point.
Negative prefix arg speaks from start of buffer to point.
 If voice lock mode is on, the paragraphs in the buffer are
voice annotated first,  see command `emacspeak-speak-voice-annotate-paragraphs'.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-other-buffer "emacspeak-speak" "\
Speak specified buffer.
Useful to listen to a buffer without switching  contexts.

\(fn BUFFER)" t nil)

(autoload 'emacspeak-speak-front-of-buffer "emacspeak-speak" "\
Speak   the buffer from start to   point" t nil)

(autoload 'emacspeak-speak-rest-of-buffer "emacspeak-speak" "\
Speak remainder of the buffer starting at point" t nil)

(autoload 'emacspeak-speak-help "emacspeak-speak" "\
Speak help buffer if one present.
With prefix arg, speaks the rest of the buffer from point.
Negative prefix arg speaks from start of buffer to point.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-minibuffer "emacspeak-speak" "\
Speak the minibuffer contents
 With prefix arg, speaks the rest of the buffer from point.
Negative prefix arg speaks from start of buffer to point.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-get-current-completion "emacspeak-speak" "\
Return the completion string under point in the *Completions* buffer." nil nil)

(autoload 'emacspeak-speak-minor-mode-line "emacspeak-speak" "\
Speak the minor mode-information.
Optional  interactive prefix arg `log-msg' logs spoken info to *Messages*.

\(fn &optional LOG-MSG)" t nil)

(autoload 'emacspeak-speak-buffer-filename "emacspeak-speak" "\
Speak name of file being visited in current buffer.
Speak default directory if invoked in a dired buffer,
or when the buffer is not visiting any file.
Interactive prefix arg `filename' speaks only the final path
component.
The result is put in the kill ring for convenience.

\(fn &optional FILENAME)" t nil)

(autoload 'emacspeak-toggle-header-line "emacspeak-speak" "\
Toggle Emacspeak's default header line." t nil)

(autoload 'emacspeak-read-previous-line "emacspeak-speak" "\
Read previous line, specified by an offset, without moving.
Default is to read the previous line. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-read-next-line "emacspeak-speak" "\
Read next line, specified by an offset, without moving.
Default is to read the next line. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-read-previous-word "emacspeak-speak" "\
Read previous word, specified as a prefix arg, without moving.
Default is to read the previous word. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-read-next-word "emacspeak-speak" "\
Read next word, specified as a numeric  arg, without moving.
Default is to read the next word. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-world-clock "emacspeak-speak" "\
Display current date and time  for specified zone.
Optional second arg `set' sets the TZ environment variable as well.

\(fn ZONE &optional SET)" t nil)

(autoload 'emacspeak-speak-time "emacspeak-speak" "\
Speak the time.
Spoken time  is available in the *notifications* buffer via C-h T.
Optional interactive prefix arg `C-u'invokes world clock.
Timezone is specified using minibuffer completion.

Second interactive prefix sets clock to new timezone.

\(fn &optional WORLD)" t nil)

(autoload 'emacspeak-speak-version "emacspeak-speak" "\
Announce version information for running emacspeak.
Optional interactive prefix arg `speak-rev' speaks only the Git revision number.

\(fn &optional SPEAK-REV)" t nil)

(autoload 'emacspeak-speak-current-kill "emacspeak-speak" "\
Speak the current kill entry.
This is the text that will be yanked in
by the next \\[yank]. Prefix numeric arg, COUNT, specifies that the
text that will be yanked as a result of a \\[yank] followed by count-1
\\[yank-pop] be spoken. The kill number that is spoken says what
numeric prefix arg to give to command yank.

\(fn &optional COUNT)" t nil)

(autoload 'emacspeak-zap-tts "emacspeak-speak" "\
Send this command to the TTS directly." t nil)

(autoload 'emacspeak-speak-current-mark "emacspeak-speak" "\
Speak the line containing the mark.
With no argument, speaks the line containing the mark--this is
where `exchange-point-and-mark' \\[exchange-point-and-mark] would
jump.  Numeric prefix arg 'COUNT' speaks line containing mark 'n'
where 'n' is one less than the number of times one has to jump
using `set-mark-command' to get to this marked position.  The
location of the mark is indicated by an aural highlight achieved
by a change in voice personality.

\(fn COUNT)" t nil)

(autoload 'emacspeak-execute-repeatedly "emacspeak-speak" "\
Execute COMMAND repeatedly.

\(fn COMMAND)" t nil)

(autoload 'emacspeak-speak-continuously "emacspeak-speak" "\
Speak a buffer continuously.
First prompts using the minibuffer for the kind of action to
perform after speaking each chunk.  E.G.  speak a line at a time
etc.  Speaking commences at current buffer position.  Pressing
\\[keyboard-quit] breaks out, leaving point on last chunk that
was spoken.  Any other key continues to speak the buffer." t nil)

(autoload 'emacspeak-speak-browse-buffer-by-style "emacspeak-speak" "\
Browse current buffer by style.
Default is to speak chunk having current style.
Interactive prefix arg `browse'  repeatedly browses  through
  chunks having same style as the current text chunk.

\(fn &optional BROWSE)" t nil)

(autoload 'emacspeak-speak-skim-buffer "emacspeak-speak" "\
Skim the current buffer  a paragraph at a time." t nil)

(autoload 'emacspeak-speak-previous-field "emacspeak-speak" "\
Move to previous field and speak it." t nil)

(autoload 'emacspeak-speak-message-again "emacspeak-speak" "\
Speak the last message from Emacs once again.
The message is also placed in the kill ring for convenient yanking
if `emacspeak-speak-message-again-should-copy-to-kill-ring' is set.

\(fn &optional FROM-MESSAGE-CACHE)" t nil)

(autoload 'emacspeak-speak-window-information "emacspeak-speak" "\
Speaks information about current window." t nil)

(autoload 'emacspeak-speak-current-window "emacspeak-speak" "\
Speak contents of current window.
Speaks entire window irrespective of point." t nil)

(autoload 'emacspeak-speak-other-window "emacspeak-speak" "\
Speak contents of `other' window.
Speaks entire window irrespective of point.
Semantics  of `other' is the same as for the builtin Emacs command
`other-window'.
Optional argument ARG  specifies `other' window to speak.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-next-window "emacspeak-speak" "\
Speak the next window." t nil)

(autoload 'emacspeak-speak-previous-window "emacspeak-speak" "\
Speak the previous window." t nil)

(autoload 'emacspeak-speak-buffer-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire buffer.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire buffer." t nil)

(autoload 'emacspeak-speak-help-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire help.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire help." t nil)

(autoload 'emacspeak-speak-line-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire line.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire line." t nil)

(autoload 'emacspeak-speak-paragraph-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire paragraph.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire paragraph." t nil)

(autoload 'emacspeak-speak-page-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire page.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire page." t nil)

(autoload 'emacspeak-speak-word-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire word.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire word." t nil)

(autoload 'emacspeak-speak-sexp-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire sexp.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire sexp." t nil)

(autoload 'emacspeak-speak-rectangle "emacspeak-speak" "\
Speak a rectangle of text.
Rectangle is delimited by point and mark.  When call from a
program, arguments specify the START and END of the rectangle.

\(fn START END)" t nil)

(autoload 'emacspeak-completions-move-to-completion-group "emacspeak-speak" "\
Move to group of choices beginning with character last
typed. If no such group exists, then we try to search for that
char, or dont move. " t nil)

(autoload 'emacspeak-mark-backward-mark "emacspeak-speak" "\
Cycle backward through the mark ring." t nil)

(autoload 'emacspeak-speak-message-at-time "emacspeak-speak" "\
Set up ring-at-time to speak message at specified time.
Provides simple stop watch functionality in addition to other things.
See documentation for command run-at-time for details on time-spec.

\(fn TIME MESSAGE)" t nil)

(autoload 'emacspeak-speak-load-directory-settings "emacspeak-speak" "\
Load a directory specific Emacspeak settings file.
This is typically used to load up settings that are specific to
an electronic book consisting of many files in the same
directory.

\(fn &optional DIR)" nil nil)

(autoload 'emacspeak-silence "emacspeak-speak" "\
Silence is golden. Stop speech, and pause/resume any media
streams. Runs `emacspeak-silence-hook' which can be used to
configure which media players get silenced or paused/resumed." t nil)

(autoload 'describe-help-keys "emacspeak-speak" "\
Show bindings under C-h." t nil)

(register-definition-prefixes "emacspeak-speak" '("emacspeak-" "ems-" "linum-mode"))

;;;***

;;;### (autoloads nil "emacspeak-speedbar" "emacspeak-speedbar.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-speedbar.el

(register-definition-prefixes "emacspeak-speedbar" '("emacspeak-speedbar-"))

;;;***

;;;### (autoloads nil "emacspeak-sudoku" "emacspeak-sudoku.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-sudoku.el

(register-definition-prefixes "emacspeak-sudoku" '("emacspeak-sudoku-"))

;;;***

;;;### (autoloads nil "emacspeak-syslog" "emacspeak-syslog.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-syslog.el

(register-definition-prefixes "emacspeak-syslog" '("emacspeak-syslog-setup"))

;;;***

;;;### (autoloads nil "emacspeak-tab-bar" "emacspeak-tab-bar.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-tab-bar.el

(register-definition-prefixes "emacspeak-tab-bar" '("emacspeak-tab-bar-speak-tab-name"))

;;;***

;;;### (autoloads nil "emacspeak-table" "emacspeak-table.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-table.el

(autoload 'emacspeak-table-make-table "emacspeak-table" "\
Construct a table object from elements.

\(fn ELEMENTS)" nil nil)

(register-definition-prefixes "emacspeak-table" '("emacspeak-table-"))

;;;***

;;;### (autoloads nil "emacspeak-table-ui" "emacspeak-table-ui.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-table-ui.el

(autoload 'emacspeak-table-prepare-table-buffer "emacspeak-table-ui" "\
Prepare tabular data.

\(fn TABLE BUFFER)" nil nil)

(autoload 'emacspeak-table-find-file "emacspeak-table-ui" "\
Open a file containing table data and display it in table mode.
emacspeak table mode is designed to let you browse tabular data using
all the power of the two-dimensional spatial layout while giving you
sufficient contextual information.  The etc/tables subdirectory of the
emacspeak distribution contains some sample tables --these are the
CalTrain schedules.  Execute command `describe-mode' bound to
\\[describe-mode] in a buffer that is in emacspeak table mode to read
the documentation on the table browser.

\(fn FILENAME)" t nil)

(autoload 'emacspeak-table-find-csv-file "emacspeak-table-ui" "\
Process a csv (comma separated values) file.
The processed  data is presented using emacspeak table navigation. 

\(fn FILENAME)" t nil)

(autoload 'emacspeak-table-view-csv-buffer "emacspeak-table-ui" "\
Process a csv (comma separated values) data.
The processed  data is  presented using emacspeak table navigation. 

\(fn &optional BUFFER-NAME)" t nil)

(autoload 'emacspeak-table-view-csv-url "emacspeak-table-ui" "\
Process a csv (comma separated values) data at  `URL'.
The processed  data is  presented using emacspeak table navigation. 

\(fn URL &optional BUFFER-NAME)" t nil)

(autoload 'emacspeak-table-display-table-in-region "emacspeak-table-ui" "\
Recognize tabular data in current region and display it in table
browsing mode in a a separate buffer.
emacspeak table mode is designed to let you browse tabular data using
all the power of the two-dimensional spatial layout while giving you
sufficient contextual information.  The tables subdirectory of the
emacspeak distribution contains some sample tables --these are the
CalTrain schedules.  Execute command `describe-mode' bound to
\\[describe-mode] in a buffer that is in emacspeak table mode to read
the documentation on the table browser.

\(fn START END)" t nil)

(register-definition-prefixes "emacspeak-table-ui" '("emacspeak-table-" "ems-csv-"))

;;;***

;;;### (autoloads nil "emacspeak-tabulate" "emacspeak-tabulate.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-tabulate.el

(autoload 'emacspeak-tabulate-region "emacspeak-tabulate" "\
Voicifies the white-space of a table if one found.  Optional interactive prefix
arg mark-fields specifies if the header row information is used to mark fields
in the white-space.

\(fn START END &optional MARK-FIELDS)" t nil)

(autoload 'ems-tabulate-parse-region "emacspeak-tabulate" "\
Parse  region as tabular data and return a vector of vectors

\(fn START END)" nil nil)

(register-definition-prefixes "emacspeak-tabulate" '("ems-"))

;;;***

;;;### (autoloads nil "emacspeak-tapestry" "emacspeak-tapestry.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-tapestry.el

(autoload 'emacspeak-tapestry-describe-tapestry "emacspeak-tapestry" "\
Describe the current layout of visible buffers in current frame.
Use interactive prefix arg to get coordinate positions of the
displayed buffers.

\(fn &optional DETAILS)" t nil)

(autoload 'emacspeak-tapestry-select-window-by-name "emacspeak-tapestry" "\
Select window by the name of the buffer it displays.
This is useful when using modes like ECB or the new GDB UI where
  you want to preserve the window layout 
but quickly switch to a window by name.

\(fn BUFFER-NAME)" t nil)

;;;***

;;;### (autoloads nil "emacspeak-tar" "emacspeak-tar.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-tar.el

(register-definition-prefixes "emacspeak-tar" '("emacspeak-tar-s"))

;;;***

;;;### (autoloads nil "emacspeak-tcl" "emacspeak-tcl.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-tcl.el

(register-definition-prefixes "emacspeak-tcl" '("tcl-"))

;;;***

;;;### (autoloads nil "emacspeak-tetris" "emacspeak-tetris.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-tetris.el

(register-definition-prefixes "emacspeak-tetris" '("emacspeak-tetris-" "tetris-move-"))

;;;***

;;;### (autoloads nil "emacspeak-texinfo" "emacspeak-texinfo.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-texinfo.el

(register-definition-prefixes "emacspeak-texinfo" '("emacspeak-texinfo-mode-hook"))

;;;***

;;;### (autoloads nil "emacspeak-threes" "emacspeak-threes.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-threes.el

(register-definition-prefixes "emacspeak-threes" '("emacspeak-threes-"))

;;;***

;;;### (autoloads nil "emacspeak-transient" "emacspeak-transient.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-transient.el

(register-definition-prefixes "emacspeak-transient" '("emacspeak-transient-"))

;;;***

;;;### (autoloads nil "emacspeak-twittering" "emacspeak-twittering.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-twittering.el

(register-definition-prefixes "emacspeak-twittering" '("emacspeak-twittering-"))

;;;***

;;;### (autoloads nil "emacspeak-url-template" "emacspeak-url-template.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-url-template.el

(autoload 'emacspeak-url-template-get "emacspeak-url-template" "\
Lookup key and return corresponding template. 

\(fn KEY)" nil nil)

(autoload 'emacspeak-url-template-open "emacspeak-url-template" "\
Fetch resource identified by URL template.

\(fn UT)" nil nil)

(autoload 'emacspeak-url-template-fetch "emacspeak-url-template" "\
Fetch a pre-defined resource.
Use Emacs completion to obtain a list of available resources.
Resources typically prompt for the relevant information
before completing the request.
Optional interactive prefix arg displays documentation for specified resource.

\(fn &optional DOCUMENTATION)" t nil)

(register-definition-prefixes "emacspeak-url-template" '("emacspeak-url-" "ems--" "gmaps-my-zip"))

;;;***

;;;### (autoloads nil "emacspeak-vdiff" "emacspeak-vdiff.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-vdiff.el

(register-definition-prefixes "emacspeak-vdiff" '("emacspeak-vdiff-"))

;;;***

;;;### (autoloads nil "emacspeak-view" "emacspeak-view.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-view.el

(register-definition-prefixes "emacspeak-view" '("emacspeak-view-"))

;;;***

;;;### (autoloads nil "emacspeak-vm" "emacspeak-vm.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-vm.el

(register-definition-prefixes "emacspeak-vm" '("emacspeak-" "ems--vm-" "vm-"))

;;;***

;;;### (autoloads nil "emacspeak-vterm" "emacspeak-vterm.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-vterm.el

(register-definition-prefixes "emacspeak-vterm" '("emacspeak-vterm-snapshot" "ems--vterm-"))

;;;***

;;;### (autoloads nil "emacspeak-vuiet" "emacspeak-vuiet.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-vuiet.el

(register-definition-prefixes "emacspeak-vuiet" '("emacspeak-vuiet-track-info"))

;;;***

;;;### (autoloads nil "emacspeak-we" "emacspeak-we.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-we.el

(defvar emacspeak-we-url-executor nil "\
URL expand/execute function  to use in current buffer.")

(define-prefix-command 'emacspeak-we-xsl-map)

(defvar emacspeak-we-xsl-p nil "\
T means we apply XSL before displaying HTML.")

(defvar emacspeak-we-xsl-transform nil "\
Specifies transform to use before displaying a page.
Default is to apply sort-tables.")

(defvar emacspeak-we-xsl-params nil "\
XSL params if any to pass to emacspeak-xslt-region.")

(autoload 'emacspeak-we-xslt-filter "emacspeak-we" "\
Extract elements matching specified XPath path locator
from Web page -- default is the current page being viewed.

\(fn PATH URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-nested-table "emacspeak-we" "\
Extract nested table specified by `table-index'. Default is to
operate on current web page when in a browser buffer; otherwise
prompt for URL. Optional arg `speak' specifies if the result should be
spoken automatically.

\(fn INDEX URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-nested-table-list "emacspeak-we" "\
Extract specified list of tables from a Web page.

\(fn TABLES URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-table-by-position "emacspeak-we" "\
Extract table at specified pos.
Default is to extract from current page.

\(fn POS URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-tables-by-position-list "emacspeak-we" "\
Extract specified list of nested tables from a WWW page.
Tables are specified by their position in the list
 of nested tables found in the page.

\(fn POSITIONS URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-table-by-match "emacspeak-we" "\
Extract table containing  specified match.
 Optional arg url specifies the page to extract content from.

\(fn MATCH URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-tables-by-match-list "emacspeak-we" "\
Extract specified  tables from a WWW page.
Tables are specified by containing  match pattern
 found in the match list.

\(fn MATCH-LIST URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-by-class "emacspeak-we" "\
Extract elements having specified class attribute from HTML. Extracts
specified elements from current WWW page and displays it in a separate
buffer. Interactive use provides list of class values as completion.

\(fn CLASS URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-junk-by-class "emacspeak-we" "\
Extract elements not having specified class attribute from HTML. Extracts
specified elements from current WWW page and displays it in a separate
buffer. Interactive use provides list of class values as completion.

\(fn CLASS URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-by-class-list "emacspeak-we" "\
Extract elements having class specified in list `classes' from HTML.
Extracts specified elements from current WWW page and displays it
in a separate buffer.  Interactive use provides list of class
values as completion. 

\(fn CLASSES URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-junk-by-class-list "emacspeak-we" "\
Extract elements not having class specified in list `classes' from HTML.
Extracts specified elements from current WWW page and displays it
in a separate buffer.  Interactive use provides list of class
values as completion. 

\(fn CLASSES URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-by-id "emacspeak-we" "\
Extract elements having specified id attribute from HTML. Extracts
specified elements from current WWW page and displays it in a separate
buffer.
Interactive use provides list of id values as completion.

\(fn ID URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-by-id-list "emacspeak-we" "\
Extract elements having id specified in list `ids' from HTML.
Extracts specified elements from current WWW page and displays it in a
separate buffer. Interactive use provides list of id values as completion. 

\(fn IDS URL &optional SPEAK)" t nil)

(defvar emacspeak-we-url-rewrite-rule nil "\
URL rewrite rule to use in current buffer.")

(autoload 'emacspeak-we-class-filter-and-follow "emacspeak-we" "\
Follow url and point, and filter the result by specified class.
Class can be set locally for a buffer, and overridden with an
interactive prefix arg. If there is a known rewrite url rule, that is
used as well.

\(fn CLASS URL &optional PROMPT)" t nil)

(autoload 'emacspeak-we-follow-and-filter-by-id "emacspeak-we" "\
Follow url and point, and filter the result by specified id.
Id can be set locally for a buffer, and overridden with an
interactive prefix arg. If there is a known rewrite url rule, that is
used as well.

\(fn ID PROMPT)" t nil)

(autoload 'emacspeak-we-style-filter "emacspeak-we" "\
Extract elements matching specified style
from HTML.  Extracts specified elements from current WWW
page and displays it in a separate buffer.  Optional arg url
specifies the page to extract contents  from.

\(fn STYLE URL &optional SPEAK)" t nil)

(defvar emacspeak-we-recent-xpath-filter "//p" "\
Caches most recently used xpath filter.")

(defvar emacspeak-we-paragraphs-xpath-filter "//p" "\
Filter paragraphs.")

(autoload 'emacspeak-we-xpath-filter-and-follow "emacspeak-we" "\
Follow url and point, and filter the result by specified xpath.
XPath can be set locally for a buffer, and overridden with an
interactive prefix arg. If there is a known rewrite url rule, that is
used as well.

\(fn &optional PROMPT)" t nil)

(autoload 'emacspeak-we-class-filter-and-follow-link "emacspeak-we" "\
Follow url and point, and filter the result by specified class.
Class can be set locally for a buffer, and overridden with an
interactive prefix arg. If there is a known rewrite url rule, that is
used as well.

\(fn &optional PROMPT)" t nil)

(autoload 'emacspeak-we-xpath-junk-and-follow "emacspeak-we" "\
Follow url and point, and filter the result by junking
elements specified by xpath.
XPath can be set locally for a buffer, and overridden with an
interactive prefix arg. If there is a known rewrite url rule, that is
used as well.

\(fn &optional PROMPT)" t nil)

(register-definition-prefixes "emacspeak-we" '("emacspeak-we-"))

;;;***

;;;### (autoloads nil "emacspeak-websearch" "emacspeak-websearch.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-websearch.el

(autoload 'emacspeak-websearch-dispatch "emacspeak-websearch" "\
 Press `?' to list available search engines.
   This interface attempts to speak the most relevant information on the result page." t nil)

(autoload 'emacspeak-websearch-gutenberg "emacspeak-websearch" "\
Perform an Gutenberg search

\(fn TYPE QUERY)" t nil)

(autoload 'emacspeak-websearch-google "emacspeak-websearch" "\
Perform a Google search.  First optional interactive prefix arg
`flag' prompts for additional search options. Second interactive
prefix arg is equivalent to hitting the I'm Feeling Lucky button on Google. 

\(fn QUERY &optional FLAG)" t nil)

(autoload 'emacspeak-websearch-accessible-google "emacspeak-websearch" "\
Use Google Lite (Experimental).
Optional prefix arg prompts for toolbelt options.

\(fn QUERY &optional OPTIONS)" t nil)

(autoload 'emacspeak-websearch-google-with-toolbelt "emacspeak-websearch" "\
Launch Google search with toolbelt.

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-ask-jeeves "emacspeak-websearch" "\
Ask Jeeves for the answer.

\(fn QUERY)" t nil)

(register-definition-prefixes "emacspeak-websearch" '("emacspeak-websearch-" "ems--websearch-google-filter"))

;;;***

;;;### (autoloads nil "emacspeak-webspace" "emacspeak-webspace.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-webspace.el
(define-prefix-command 'emacspeak-webspace 'emacspeak-webspace-keymap)

(autoload 'emacspeak-webspace-headlines "emacspeak-webspace" "\
Startup Headlines ticker using RSS/Atom  feeds." t nil)

(autoload 'emacspeak-webspace-feed-reader "emacspeak-webspace" "\
Display Feed Reader Feed list in a WebSpace buffer.
Optional interactive prefix arg forces a refresh.

\(fn &optional REFRESH)" t nil)

(register-definition-prefixes "emacspeak-webspace" '("emacspeak-webspace-"))

;;;***

;;;### (autoloads nil "emacspeak-widget" "emacspeak-widget.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-widget.el

(autoload 'emacspeak-widget-summarize-parent "emacspeak-widget" "\
Summarize parent of widget at point." t nil)

(autoload 'emacspeak-widget-summarize "emacspeak-widget" "\
Summarize specified widget.

\(fn WIDGET)" nil nil)

(autoload 'emacspeak-widget-default-summarize "emacspeak-widget" "\
Fall back summarizer for all widgets

\(fn WIDGET)" nil nil)

(register-definition-prefixes "emacspeak-widget" '("emacspeak-widget-"))

;;;***

;;;### (autoloads nil "emacspeak-wizards" "emacspeak-wizards.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-wizards.el

(autoload 'emacspeak-speak-popup-messages "emacspeak-wizards" "\
Pop up messages buffer.
If it is already selected then hide it and try to restore
previous window configuration." t nil)

(autoload 'emacspeak-speak-telephone-directory "emacspeak-wizards" "\
Lookup and display a phone number.
With prefix arg, opens the phone book for editing.

\(fn &optional EDIT)" t nil)

(autoload 'emacspeak-wizards-find-file-as-root "emacspeak-wizards" "\
Like `ido-find-file, but automatically edit the file with
root-privileges (using tramp/sudo), if the file is not writable by
user.

\(fn FILE)" t nil)

(autoload 'emacspeak-learn-emacs-mode "emacspeak-wizards" "\
Helps you learn the keys.  You can press keys and hear what they do.
To leave, press \\[keyboard-quit]." t nil)

(autoload 'emacspeak-frame-label-or-switch-to-labelled-frame "emacspeak-wizards" "\
Switch to labelled frame.
With optional PREFIX argument, label current frame.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-next-frame-or-buffer "emacspeak-wizards" "\
Move to next buffer.
With optional interactive prefix arg `frame', move to next frame instead.

\(fn &optional FRAME)" t nil)

(autoload 'emacspeak-previous-frame-or-buffer "emacspeak-wizards" "\
Move to previous buffer.
With optional interactive prefix arg `frame', move to previous frame instead.

\(fn &optional FRAME)" t nil)

(autoload 'emacspeak-speak-this-buffer-other-window-display "emacspeak-wizards" "\
Speak this buffer as displayed in a different frame.  Emacs
allows you to display the same buffer in multiple windows or
frames.  These different windows can display different
portions of the buffer.  This is equivalent to leaving a
book open at places at once.  This command allows you to
listen to the places where you have left the book open.  The
number used to invoke this command specifies which of the
displays you wish to speak.  Typically you will have two or
at most three such displays open.  The current display is 0,
the next is 1, and so on.  Optional argument ARG specifies
the display to speak.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-this-buffer-previous-display "emacspeak-wizards" "\
Speak this buffer as displayed in a `previous' window.
See documentation for command
`emacspeak-speak-this-buffer-other-window-display' for the
meaning of `previous'." t nil)

(autoload 'emacspeak-speak-this-buffer-next-display "emacspeak-wizards" "\
Speak this buffer as displayed in a `previous' window.
See documentation for command
`emacspeak-speak-this-buffer-other-window-display' for the
meaning of `next'." t nil)

(autoload 'emacspeak-select-this-buffer-other-window-display "emacspeak-wizards" "\
Switch  to this buffer as displayed in a different frame.  Emacs
allows you to display the same buffer in multiple windows or
frames.  These different windows can display different
portions of the buffer.  This is equivalent to leaving a
book open at multiple places at once.  This command allows you to
move to the places where you have left the book open.  The
number used to invoke this command specifies which of the
displays you wish to select.  Typically you will have two or
at most three such displays open.  The current display is 0,
the next is 1, and so on.  Optional argument ARG specifies
the display to select.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-select-this-buffer-previous-display "emacspeak-wizards" "\
Select this buffer as displayed in a `previous' window.
See documentation for command
`emacspeak-select-this-buffer-other-window-display' for the
meaning of `previous'." t nil)

(autoload 'emacspeak-select-this-buffer-next-display "emacspeak-wizards" "\
Select this buffer as displayed in a `next' frame.
See documentation for command
`emacspeak-select-this-buffer-other-window-display' for the
meaning of `next'." t nil)

(autoload 'emacspeak-clipboard-copy "emacspeak-wizards" "\
Copy contents of the region to the emacspeak clipboard. Previous
contents of the clipboard will be overwritten. The Emacspeak clipboard
is a convenient way of sharing information between independent
Emacspeak sessions running on the same or different machines. Do not
use this for sharing information within an Emacs session --Emacs'
register commands are far more efficient and light-weight. Optional
interactive prefix arg results in Emacspeak prompting for the
clipboard file to use. Argument START and END specifies
region. Optional argument PROMPT specifies whether we prompt for the
name of a clipboard file.

\(fn START END &optional PROMPT)" t nil)

(autoload 'emacspeak-clipboard-paste "emacspeak-wizards" "\
Yank contents of the Emacspeak clipboard at point.
The Emacspeak clipboard is a convenient way of sharing information between
independent Emacspeak sessions running on the same or different
machines.  Do not use this for sharing information within an Emacs
session --Emacs' register commands are far more efficient and
light-weight.  Optional interactive prefix arg pastes from
the emacspeak table clipboard instead.

\(fn &optional PASTE-TABLE)" t nil)

(autoload 'emacspeak-wizards-show-eval-result "emacspeak-wizards" "\
Convenience command to pretty-print and view Lisp evaluation results.

\(fn FORM)" t nil)

(autoload 'emacspeak-emergency-tts-restart "emacspeak-wizards" "\
For use in an emergency.
Will start TTS engine specified by
emacspeak-emergency-tts-server." t nil)

(autoload 'emacspeak-ssh-tts-restart "emacspeak-wizards" "\
Restart specified ssh tts server." t nil)

(autoload 'emacspeak-show-style-at-point "emacspeak-wizards" "\
Show value of property personality (and possibly face) at point." t nil)

(autoload 'emacspeak-show-property-at-point "emacspeak-wizards" "\
Show value of PROPERTY at point.
If optional arg property is not supplied, read it interactively.
Provides completion based on properties at point.
If no property is set, show a message and exit.

\(fn &optional PROPERTY)" t nil)

(autoload 'emacspeak-skip-blank-lines-forward "emacspeak-wizards" "\
Move forward across blank lines.
The line under point is then spoken.
Signals end of buffer." t nil)

(autoload 'emacspeak-skip-blank-lines-backward "emacspeak-wizards" "\
Move backward  across blank lines.
The line under point is   then spoken.
Signals beginning  of buffer." t nil)

(autoload 'emacspeak-wizards-terminal "emacspeak-wizards" "\
Launch terminal and rename buffer appropriately.

\(fn PROGRAM)" t nil)

(autoload 'emacspeak-annotate-add-annotation "emacspeak-wizards" "\
Add annotation to the annotation working buffer.
Prompt for annotation buffer if not already set.
Interactive prefix arg `reset' prompts for the annotation
buffer even if one is already set.
Annotation is entered in a temporary buffer and the
annotation is inserted into the working buffer when complete.

\(fn &optional RESET)" t nil)

(autoload 'emacspeak-wizards-shell-toggle "emacspeak-wizards" "\
Switch to the shell buffer and cd to
 the directory of the previously current buffer." t nil)

(autoload 'emacspeak-wizards-generate-finder "emacspeak-wizards" "\
Generate a widget-enabled finder wizard." t nil)

(autoload 'emacspeak-wizards-finder-find "emacspeak-wizards" "\
Run find-dired on specified switches after prompting for the
directory to where find is to be launched.

\(fn DIRECTORY)" t nil)

(autoload 'emacspeak-customize "emacspeak-wizards" "\
Customize Emacspeak." t nil)

(autoload 'emacspeak-wizards-squeeze-blanks "emacspeak-wizards" "\
Squeeze multiple blank lines in current buffer.

\(fn START END)" t nil)

(autoload 'emacspeak-wizards-count-slides-in-region "emacspeak-wizards" "\
Count slides starting from point.

\(fn START END)" t nil)

(autoload 'emacspeak-wizards-how-many-matches "emacspeak-wizards" "\
If you define a file local variable
called `emacspeak-occur-pattern' that holds a regular expression
that matches  lines of interest, you can use this command to conveniently
run `how-many' to count  matching header lines.
With interactive prefix arg, prompts for and remembers the file local pattern.

\(fn START END &optional PREFIX)" t nil)

(autoload 'emacspeak-wizards-occur-header-lines "emacspeak-wizards" "\
If you define a file local variable called
`emacspeak-occur-pattern' that holds a regular expression that
matches header lines, you can use this command to conveniently
run `occur' to find matching header lines. With prefix arg,
prompts for and sets value of the file local pattern.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-kill-buffer-quietly "emacspeak-wizards" "\
Kill current buffer without asking for confirmation." t nil)

(autoload 'emacspeak-wizards-fix-typo "emacspeak-wizards" "\
Search and replace  recursively in all files with extension `ext'
for `word' and replace it with correction.
Use with caution.

\(fn EXT WORD CORRECTION)" t nil)

(autoload 'emacspeak-wizards-vc-viewer "emacspeak-wizards" "\
View contents of specified virtual console.

\(fn CONSOLE)" t nil)

(autoload 'emacspeak-wizards-vc-n "emacspeak-wizards" "\
Accelerator for VC viewer." t nil)

(autoload 'emacspeak-wizards-find-longest-line-in-region "emacspeak-wizards" "\
Find longest line in region.
Moves to the longest line when called interactively.

\(fn START END)" t nil)

(autoload 'emacspeak-wizards-find-longest-paragraph-in-region "emacspeak-wizards" "\
Find longest paragraph in region.
Moves to the longest paragraph when called interactively.

\(fn START END)" t nil)

(autoload 'emacspeak-wizards-find-grep "emacspeak-wizards" "\
Run compile using find and grep.
Interactive  arguments specify filename pattern and search pattern.

\(fn GLOB PATTERN)" t nil)

(autoload 'emacspeak-wizards-show-face "emacspeak-wizards" "\
Show salient properties of specified face.

\(fn FACE)" t nil)

(autoload 'emacspeak-wizards-speak-iso-datetime "emacspeak-wizards" "\
Make ISO date-time speech friendly.

\(fn ISO)" t nil)

(autoload 'emacspeak-wizards-toggle-mm-dd-yyyy-date-pronouncer "emacspeak-wizards" "\
Toggle pronunciation of mm-dd-yyyy dates." t nil)

(autoload 'emacspeak-wizards-toggle-yyyymmdd-date-pronouncer "emacspeak-wizards" "\
Toggle pronunciation of yyyymmdd  dates." t nil)

(autoload 'emacspeak-wizards-units "emacspeak-wizards" "\
Run units in a comint sub-process." t nil)

(autoload 'emacspeak-wizards-shell "emacspeak-wizards" "\
Run Emacs built-in `shell' command when not in a shell buffer, or
when called with a prefix argument. When called from a shell buffer,
switches to `next' shell buffer. When called from outside a shell
buffer, find the most `appropriate shell' and switch to it. Once
switched, set default directory in that target shell to the directory
of the source buffer.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-wizards-shell-by-key "emacspeak-wizards" "\
Switch to shell buffer by key. This provides a predictable
  means for switching to a specific shell buffer. When invoked
  from a non-shell-mode buffer that is a dired-buffer or is
  visiting a file, invokes `cd ' in the shell to change to the
  value of `default-directory' --- if called with a
  prefix-arg. When already in a shell buffer, interactive prefix
  arg `prefix' causes this shell to be re-keyed if appropriate
  --- see \\[emacspeak-wizards-shell-re-key] for an explanation
  of how re-keying works.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-wizards-project-shells-initialize "emacspeak-wizards" "\
Create shells per `emacspeak-wizards-project-shells'." nil nil)

(autoload 'emacspeak-wizards-shell-directory-reset "emacspeak-wizards" "\
Set current directory to this shell's initial directory if one was defined." t nil)

(autoload 'emacspeak-wizards-braille "emacspeak-wizards" "\
Insert Braille string at point.

\(fn S)" t nil)

(autoload 'emacspeak-wizards-cycle-to-previous-buffer "emacspeak-wizards" "\
Cycles to previous buffer having same mode." t nil)

(autoload 'emacspeak-wizards-cycle-to-next-buffer "emacspeak-wizards" "\
Cycles to next buffer having same mode." t nil)

(autoload 'emacspeak-wizards-term "emacspeak-wizards" "\
Switch to an ansi-term buffer or create one.
With prefix arg, always creates a new terminal.
Otherwise cycles through existing terminals, creating the first
term if needed.

\(fn CREATE)" t nil)

(autoload 'emacspeak-wizards-espeak-string "emacspeak-wizards" "\
Speak string in lang via ESpeak.
Lang is obtained from property `lang' on string, or  via an interactive prompt.

\(fn STRING)" t nil)

(autoload 'emacspeak-wizards-espeak-region "emacspeak-wizards" "\
Speak region using ESpeak polyglot wizard.

\(fn START END)" t nil)

(autoload 'emacspeak-wizards-espeak-line "emacspeak-wizards" "\
Speak line using espeak polyglot wizard." t nil)

(autoload 'emacspeak-wizards-enumerate-uncovered-commands "emacspeak-wizards" "\
Enumerate unadvised commands matching pattern.
Optional interactive prefix arg `bound-only'
filters out commands that dont have an active key-binding.

\(fn PATTERN &optional BOUND-ONLY)" t nil)

(autoload 'emacspeak-wizards-enumerate-unmapped-faces "emacspeak-wizards" "\
Enumerate unmapped faces matching pattern.

\(fn &optional PATTERN)" t nil)

(autoload 'emacspeak-wizards-execute-emacspeak-command "emacspeak-wizards" "\
Prompt for and execute an Emacspeak command.

\(fn COMMAND)" t nil)

(autoload 'emacspeak-wizards-shell-command-on-current-file "emacspeak-wizards" "\
Prompts for and runs shell command on current file.

\(fn COMMAND)" t nil)

(autoload 'emacspeak-wizards-view-buffers-filtered-by-this-mode "emacspeak-wizards" "\
Buffer menu filtered by  mode of current-buffer." t nil)

(autoload 'emacspeak-wizards-view-buffers-filtered-by-m-player-mode "emacspeak-wizards" "\
Buffer menu filtered by  m-player mode." t nil)

(autoload 'emacspeak-wizards-eww-buffer-list "emacspeak-wizards" "\
Display list of open EWW buffers." t nil)

(autoload 'emacspeak-wizards-tune-in-radio-browse "emacspeak-wizards" "\
Browse Tune-In Radio.
Optional interactive prefix arg `category' prompts for a category.

\(fn &optional CATEGORY)" t nil)

(autoload 'emacspeak-wizards-tune-in-radio-search "emacspeak-wizards" "\
Search Tune-In Radio." t nil)

(autoload 'emacspeak-wizards-quote "emacspeak-wizards" "\
Top-level dispatch for looking up Stock Market information.

Key : Action
f   :  Financials
m   :  Account metadata 
n   :  News
p   :  Price
q   :  Quotes
t   :  tops/last 

\(fn &optional REFRESH)" t nil)

(autoload 'emacspeak-wizards-set-colors "emacspeak-wizards" "\
Interactively prompt for foreground and background colors." t nil)

(autoload 'emacspeak-wizards-color-diff-at-point "emacspeak-wizards" "\
Meaningfully speak difference between background and foreground color at point.
With interactive prefix arg, set foreground and background color first.

\(fn &optional SET)" t nil)

(autoload 'emacspeak-wizards-colors "emacspeak-wizards" "\
Display list of colors and setup a callback to activate color
under point as either the foreground or background color." t nil)

(autoload 'emacspeak-wizards-color-at-point "emacspeak-wizards" "\
Echo foreground/background color at point." t nil)

(autoload 'emacspeak-wizards-swap-fg-and-bg "emacspeak-wizards" "\
Swap foreground and background." t nil)

(autoload 'emacspeak-wizards-noaa-weather "emacspeak-wizards" "\
Display weather information using NOAA Weather API.
Data is retrieved only once, subsequent calls switch to previously
displayed results. Kill that buffer or use an interactive prefix
arg (C-u) to get new data.  Optional second interactive prefix
arg (C-u C-u) asks for location address; Default is to display
weather for `gmaps-my-address'.  

\(fn &optional ASK)" t nil)

(autoload 'emacspeak-wizards-google-news "emacspeak-wizards" "\
Clean up news.google.com for  skimming the news." t nil)

(autoload 'emacspeak-wizards-google-headlines "emacspeak-wizards" "\
Display just the headlines from Google News for fast loading." t nil)

(autoload 'emacspeak-wizards-execute-asynchronously "emacspeak-wizards" "\
Read key-sequence, then execute its command on a new thread.

\(fn KEY)" t nil)

(autoload 'emacspeak-wizards-midi-using-m-score "emacspeak-wizards" "\
Play midi file using mscore from musescore package.

\(fn MIDI-FILE)" t nil)

(autoload 'emacspeak-wizards-ytel-play-at-point "emacspeak-wizards" "\
Play video. Argument `id' is the video-id.
Play current video in ytel when called interactively.
Optional interactive prefix arg `best' picks best audio format.

\(fn ID &optional BEST)" t nil)

(register-definition-prefixes "emacspeak-wizards" '("emacspeak-" "ems-" "voice-setup-"))

;;;***

;;;### (autoloads nil "emacspeak-xkcd" "emacspeak-xkcd.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-xkcd.el

(register-definition-prefixes "emacspeak-xkcd" '("emacspeak-xkcd-" "xkcd-transcript"))

;;;***

;;;### (autoloads nil "emacspeak-xslt" "emacspeak-xslt.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-xslt.el

(defsubst emacspeak-xslt-get (style) "\
Return fully qualified stylesheet path." (expand-file-name style emacspeak-xslt-directory))

(defconst emacspeak-opml-view-xsl (eval-when-compile (emacspeak-xslt-get "opml.xsl")) "\
XSL stylesheet used for viewing OPML  Feeds.")

(defconst emacspeak-rss-view-xsl (eval-when-compile (emacspeak-xslt-get "rss.xsl")) "\
XSL stylesheet used for viewing RSS Feeds.")

(defconst emacspeak-atom-view-xsl (eval-when-compile (emacspeak-xslt-get "atom.xsl")) "\
XSL stylesheet used for viewing Atom Feeds.")

(defvar emacspeak-xslt-program (executable-find "xsltproc") "\
Name of XSLT transformation engine.")

(autoload 'emacspeak-xslt-region "emacspeak-xslt" "\
Apply XSLT transformation to region and replace it with
the result.  This uses XSLT processor xsltproc available as
part of the libxslt package.

\(fn XSL START END &optional PARAMS NO-COMMENT)" nil nil)

(autoload 'emacspeak-xslt-url "emacspeak-xslt" "\
Apply XSLT transformation to url
and return the results in a newly created buffer.
  This uses XSLT processor xsltproc available as
part of the libxslt package.

\(fn XSL URL &optional PARAMS NO-COMMENT)" nil nil)

(autoload 'emacspeak-xslt-xml-url "emacspeak-xslt" "\
Apply XSLT transformation to XML url
and return the results in a newly created buffer.
  This uses XSLT processor xsltproc available as
part of the libxslt package.

\(fn XSL URL &optional PARAMS)" nil nil)

(register-definition-prefixes "emacspeak-xslt" '("emacspeak-xslt-"))

;;;***

;;;### (autoloads nil "emacspeak-ytel" "emacspeak-ytel.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-ytel.el

(register-definition-prefixes "emacspeak-ytel" '("emacspeak-ytel-"))

;;;***

;;;### (autoloads nil "espeak-voices" "espeak-voices.el" (0 0 0 0))
;;; Generated autoloads from espeak-voices.el

(autoload 'espeak "espeak-voices" "\
Start ESpeak engine." t nil)

(autoload 'espeak-configure-tts "espeak-voices" "\
Configure TTS environment to use eSpeak." nil nil)

(register-definition-prefixes "espeak-voices" '("espeak-" "tts-default-voice"))

;;;***

;;;### (autoloads nil "extra-muggles" "extra-muggles.el" (0 0 0 0))
;;; Generated autoloads from extra-muggles.el

(autoload 'emacspeak-muggles-generate "extra-muggles" "\
Generate a Muggle from specified k-map.
Argument `k-map' is a symbol  that names a keymap.

\(fn K-MAP)" nil nil)

(register-definition-prefixes "extra-muggles" '("emacspeak-muggles-"))

;;;***

;;;### (autoloads nil "g-utils" "g-utils.el" (0 0 0 0))
;;; Generated autoloads from g-utils.el

(autoload 'g-html-string "g-utils" "\
Return formatted string.

\(fn HTML-STRING)" nil nil)

(register-definition-prefixes "g-utils" '("g-"))

;;;***

;;;### (autoloads nil "gm-nnir" "gm-nnir.el" (0 0 0 0))
;;; Generated autoloads from gm-nnir.el

(autoload 'gm-nnir-group-make-nnir-group "gm-nnir" "\
GMail equivalent of gnus-group-make-nnir-group.
This uses standard IMap search operators.

\(fn Q)" t nil)

(autoload 'gm-nnir-group-make-gmail-group "gm-nnir" "\
Use GMail search syntax exclusively.
See https://support.google.com/mail/answer/7190?hl=en for syntax.
 note: nnimap-address etc are available as local vars if needed in these functions.

\(fn QUERY)" t nil)

(register-definition-prefixes "gm-nnir" '("gm-nnir-"))

;;;***

;;;### (autoloads nil "gmaps" "gmaps.el" (0 0 0 0))
;;; Generated autoloads from gmaps.el

(autoload 'gmaps-address-location "gmaps" "\
Returns gmaps--location structure. Memoized to save network calls.

\(fn ADDRESS)" nil nil)

(autoload 'gmaps-address-geocode "gmaps" "\
Return lat/long for a given address.

\(fn ADDRESS)" nil nil)

(autoload 'gmaps-geocode "gmaps" "\
Geocode given address.
Optional argument `raw-p' returns complete JSON  object.

\(fn ADDRESS &optional RAW-P)" nil nil)

(autoload 'gmaps-reverse-geocode "gmaps" "\
Reverse geocode lat-long.
Optional argument `raw-p' returns raw JSON  object.

\(fn LAT-LONG &optional RAW-P)" nil nil)

(defvar gmaps-my-address nil "\
Location address. Setting this updates gmaps-my-location coordinates  via geocoding.")

(custom-autoload 'gmaps-my-address "gmaps" nil)

(autoload 'gmaps "gmaps" "\
Google Maps Interaction." t nil)

(register-definition-prefixes "gmaps" '("gmaps-"))

;;;***

;;;### (autoloads nil "gweb" "gweb.el" (0 0 0 0))
;;; Generated autoloads from gweb.el

(register-definition-prefixes "gweb" '("gweb-"))

;;;***

;;;### (autoloads nil "ladspa" "ladspa.el" (0 0 0 0))
;;; Generated autoloads from ladspa.el

(defconst ladspa-home (or (getenv "LADSPA_PATH") "/usr/lib/ladspa") "\
Instalation location for Ladspa plugins.")

(autoload 'ladspa "ladspa" "\
Launch Ladspa workbench.

\(fn &optional REFRESH)" t nil)

(register-definition-prefixes "ladspa" '("ladspa-"))

;;;***

;;;### (autoloads nil "mac-voices" "mac-voices.el" (0 0 0 0))
;;; Generated autoloads from mac-voices.el

(autoload 'mac-configure-tts "mac-voices" "\
Configure TTS environment to use mac  family of synthesizers." nil nil)

(register-definition-prefixes "mac-voices" '("mac-"))

;;;***

;;;### (autoloads nil "new-kbd" "new-kbd.el" (0 0 0 0))
;;; Generated autoloads from new-kbd.el

(register-definition-prefixes "new-kbd" '("ems--kbd-" "new-kbd"))

;;;***

;;;### (autoloads nil "nm" "nm.el" (0 0 0 0))
;;; Generated autoloads from nm.el

(register-definition-prefixes "nm" '("nm-"))

;;;***

;;;### (autoloads nil "outloud-voices" "outloud-voices.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from outloud-voices.el

(autoload 'outloud-configure-tts "outloud-voices" "\
Configure TTS environment to use Outloud." nil nil)

(register-definition-prefixes "outloud-voices" '("outloud"))

;;;***

;;;### (autoloads nil "plain-voices" "plain-voices.el" (0 0 0 0))
;;; Generated autoloads from plain-voices.el

(autoload 'plain-configure-tts "plain-voices" "\
Configures TTS environment to use Plain family of synthesizers." nil nil)

(register-definition-prefixes "plain-voices" '("plain"))

;;;***

;;;### (autoloads nil "soundscape" "soundscape.el" (0 0 0 0))
;;; Generated autoloads from soundscape.el

(autoload 'soundscape "soundscape" "\
Play soundscape.

\(fn SCAPE)" t nil)

(autoload 'soundscape-init "soundscape" "\
Initialize Soundscape module." nil nil)

(autoload 'soundscape-listener "soundscape" "\
Start  a Soundscape listener.
Listener is loaded with all Soundscapes defined in `soundscape-default-theme' .
Optional interactive prefix arg restarts the listener if already running.

\(fn &optional RESTART)" t nil)

(autoload 'soundscape-toggle "soundscape" "\
Toggle automatic SoundScapes.
When turned on, Soundscapes are automatically run based on current major mode.
Run command \\[soundscape-theme] to see the default mode->mood mapping." t nil)

(register-definition-prefixes "soundscape" '("soundscape-"))

;;;***

;;;### (autoloads nil "sox" "sox.el" (0 0 0 0))
;;; Generated autoloads from sox.el

(autoload 'sox "sox" "\
Create a new Audio Workbench or switch to an existing workbench." t nil)

(autoload 'sox-play "sox" "\
Play sound from current context." t nil)

(register-definition-prefixes "sox" '("sox-"))

;;;***

;;;### (autoloads nil "sox-gen" "sox-gen.el" (0 0 0 0))
;;; Generated autoloads from sox-gen.el

(autoload 'sox-binaural "sox-gen" "\
Play specified binaural effect.

\(fn NAME DURATION)" t nil)

(autoload 'sox-rev-up "sox-gen" "\
Play rev-up set of  binaural beats.
Param `length' specifies total duration.

\(fn LENGTH)" t nil)

(autoload 'sox-turn-down "sox-gen" "\
Play turn-down set of  binaural beats.
Param `length' specifies total duration.

\(fn LENGTH)" t nil)

(autoload 'sox-wind-down "sox-gen" "\
Play wind-down set of  binaural beats.
Param `length' specifies total duration.

\(fn LENGTH)" t nil)

(autoload 'sox-relax "sox-gen" "\
Play relax set of  binaural beats.
Param `length' specifies total duration.

\(fn LENGTH)" t nil)

(autoload 'sox-chakras "sox-gen" "\
Play each chakra for specified duration.
Parameter `theme' specifies variant.

\(fn THEME DURATION)" t nil)

(autoload 'sox-multiwindow "sox-gen" "\
Produce a short note used to cue multiwindow.

\(fn &optional SWAP SPEED)" nil nil)

(autoload 'sox-do-scroll-up "sox-gen" "\
Produce a short do-scroll-up.

\(fn &optional SPEED)" nil nil)

(autoload 'sox-do-scroll-down "sox-gen" "\
Produce a short do-scroll-down.

\(fn &optional SPEED)" nil nil)

(autoload 'sox-tones "sox-gen" "\
Play sequence of tones --- optional args tempo and speed default to 1.

\(fn &optional TEMPO SPEED)" nil nil)

(register-definition-prefixes "sox-gen" '("sox-"))

;;;***

;;;### (autoloads nil "tapestry" "tapestry.el" (0 0 0 0))
;;; Generated autoloads from tapestry.el

(autoload 'tapestry "tapestry" "\
Returns a list containing complete information about the current
configuration of Emacs frames, windows, buffers and cursor
positions.  Call the function set-tapestry with the list that this function
returns to restore the configuration.

Optional first arg FRAME-LIST should be a list of frames; only
configuration information about these frames will be returned.

The configuration information is returned in a form that can be saved and
restored across multiple Emacs sessions.

\(fn &optional FRAME-LIST)" nil nil)

(autoload 'set-tapestry "tapestry" "\
Restore the frame/window/buffer configuration described by MAP,
which should be a list previously returned by a call to
tapestry.

Optional second arg N causes frame reconfiguration to be skipped
and the windows of the current frame will configured according to
the window map of the Nth frame in MAP.

Optional third arg ROOT-WINDOW-EDGES non-nil should be a list
containing the edges of a window in the current frame.  This list
should be in the same form as returned by the `window-edges'
function.  The window configuration from MAP will be restored in
this window.  If no window with these exact edges exists, a
window that lies entirely within the edge coordinates will be
expanded until the edge coordinates match or the window bounded by
ROOT-WINDOW-EDGES is entirely contained within the expanded
window.  If no window entirely within the ROOT-WINDOW-EDGES edge
coordinates can be found, the window with the greatest overlap of
ROOT-WINDOW-EDGES will be used.

\(fn MAP &optional N ROOT-WINDOW-EDGES)" nil nil)

(autoload 'tapestry-remove-frame-parameters "tapestry" "\


\(fn MAP PARAMS)" nil nil)

(autoload 'tapestry-nullify-tapestry-elements "tapestry" "\


\(fn MAP &optional BUF-FILE-NAME BUF-NAME WINDOW-START WINDOW-POINT WINDOW-HSCROLL SELECTED-WINDOW)" nil nil)

(autoload 'tapestry-replace-tapestry-element "tapestry" "\


\(fn MAP WHAT FUNCTION)" nil nil)

(register-definition-prefixes "tapestry" '("tapestry-"))

;;;***

;;;### (autoloads nil "tetris" "tetris.el" (0 0 0 0))
;;; Generated autoloads from tetris.el

(autoload 'tetris "tetris" "\
Play the Tetris game.
Shapes drop from the top of the screen, and the user has to move and
rotate the shape to fit in with those at the bottom of the screen so
as to form complete rows.

tetris-mode keybindings:
   \\<tetris-mode-map>
\\[tetris-start-game]   Starts a new game of Tetris
\\[tetris-end-game]     Terminates the current game
\\[tetris-pause-game]   Pauses (or resumes) the current game
\\[tetris-move-left]    Moves the shape one square to the left
\\[tetris-move-right]   Moves the shape one square to the right
\\[tetris-rotate-prev]  Rotates the shape clockwise
\\[tetris-rotate-next]  Rotates the shape anticlockwise
\\[tetris-move-bottom]  Drops the shape to the bottom of the playing area

" t nil)

(register-definition-prefixes "tetris" '("tetris-"))

;;;***

;;;### (autoloads nil "toy-braille" "toy-braille.el" (0 0 0 0))
;;; Generated autoloads from toy-braille.el

(autoload 'get-toy-braille-string "toy-braille" "\


\(fn INSTR)" nil nil)

(register-definition-prefixes "toy-braille" '("toy-braille-map"))

;;;***

;;;### (autoloads nil "voice-setup" "voice-setup.el" (0 0 0 0))
;;; Generated autoloads from voice-setup.el

(autoload 'voice-lock-mode "voice-setup" "\
Toggle voice lock mode.

If called interactively, toggle `Voice-Lock mode'.  If the prefix
argument is positive, enable the mode, and if it is zero or
negative, disable the mode.

If called from Lisp, toggle the mode if ARG is `toggle'.  Enable
the mode if ARG is nil, omitted, or is a positive number.
Disable the mode if ARG is a negative number.

The mode's hook is called both when the mode is enabled and when
it is disabled.

\(fn &optional ARG)" t nil)

(put 'global-voice-lock-mode 'globalized-minor-mode t)

(defvar global-voice-lock-mode t "\
Non-nil if Global Voice-Lock mode is enabled.
See the `global-voice-lock-mode' command
for a description of this minor mode.
Setting this variable directly does not take effect;
either customize it (see the info node `Easy Customization')
or call the function `global-voice-lock-mode'.")

(custom-autoload 'global-voice-lock-mode "voice-setup" nil)

(autoload 'global-voice-lock-mode "voice-setup" "\
Toggle Voice-Lock mode in all buffers.
With prefix ARG, enable Global Voice-Lock mode if ARG is positive;
otherwise, disable it.  If called from Lisp, enable the mode if
ARG is omitted or nil.

Voice-Lock mode is enabled in all buffers where
`voice-lock-mode--turn-on' would do it.

See `voice-lock-mode' for more information on
Voice-Lock mode.

\(fn &optional ARG)" t nil)

(autoload 'voice-setup-toggle-silence-personality "voice-setup" "\
Toggle audibility of personality under point  . " t nil)

(register-definition-prefixes "voice-setup" '("defvoice" "voice-"))

;;;***

;;;### (autoloads nil "xbacklight" "xbacklight.el" (0 0 0 0))
;;; Generated autoloads from xbacklight.el

(autoload 'xbacklight-get "xbacklight" "\
Get current brightness level." t nil)

(autoload 'xbacklight-set "xbacklight" "\
Set brightness to  specified level.
`brightness' is a percentage value.

\(fn BRIGHTNESS)" t nil)

(autoload 'xbacklight-increment "xbacklight" "\
Increase brightness by  by one step." t nil)

(autoload 'xbacklight-decrement "xbacklight" "\
Decrease brightness by  by one step." t nil)

(autoload 'xbacklight-black "xbacklight" "\
Turn screen black." t nil)

(autoload 'xbacklight-white "xbacklight" "\
Turn screen white." t nil)

(register-definition-prefixes "xbacklight" '("xbacklight-"))

;;;***

;;;### (autoloads nil nil ("emacspeak-abc-mode.el" "emacspeak-add-log.el"
;;;;;;  "emacspeak-apt-sources.el" "emacspeak-bbdb.el" "emacspeak-bibtex.el"
;;;;;;  "emacspeak-bookmark.el" "emacspeak-browse-kill-ring.el" "emacspeak-calc.el"
;;;;;;  "emacspeak-cider.el" "emacspeak-ciel.el" "emacspeak-clojure.el"
;;;;;;  "emacspeak-cmuscheme.el" "emacspeak-cperl.el" "emacspeak-deadgrep.el"
;;;;;;  "emacspeak-debugger.el" "emacspeak-desktop.el" "emacspeak-dictionary.el"
;;;;;;  "emacspeak-diff-mode.el" "emacspeak-dumb-jump.el" "emacspeak-eclim.el"
;;;;;;  "emacspeak-eglot.el" "emacspeak-elisp-refs.el" "emacspeak-elpy.el"
;;;;;;  "emacspeak-elscreen.el" "emacspeak-epa.el" "emacspeak-ess.el"
;;;;;;  "emacspeak-flycheck.el" "emacspeak-flymake.el" "emacspeak-folding.el"
;;;;;;  "emacspeak-forge.el" "emacspeak-geiser.el" "emacspeak-gnuplot.el"
;;;;;;  "emacspeak-go-mode.el" "emacspeak-gtags.el" "emacspeak-gud.el"
;;;;;;  "emacspeak-haskell.el" "emacspeak-hide-lines.el" "emacspeak-hideshow.el"
;;;;;;  "emacspeak-iedit.el" "emacspeak-indium.el" "emacspeak-jdee.el"
;;;;;;  "emacspeak-kmacro.el" "emacspeak-lua.el" "emacspeak-make-mode.el"
;;;;;;  "emacspeak-markdown.el" "emacspeak-metapost.el" "emacspeak-muse.el"
;;;;;;  "emacspeak-navi-mode.el" "emacspeak-net-utils.el" "emacspeak-orgalist.el"
;;;;;;  "emacspeak-perl.el" "emacspeak-preamble.el" "emacspeak-project.el"
;;;;;;  "emacspeak-py.el" "emacspeak-pydoc.el" "emacspeak-python.el"
;;;;;;  "emacspeak-racer.el" "emacspeak-racket.el" "emacspeak-re-builder.el"
;;;;;;  "emacspeak-reftex.el" "emacspeak-related.el" "emacspeak-rg.el"
;;;;;;  "emacspeak-rst.el" "emacspeak-ruby.el" "emacspeak-sgml-mode.el"
;;;;;;  "emacspeak-sh-script.el" "emacspeak-slime.el" "emacspeak-smart-window.el"
;;;;;;  "emacspeak-smartparens.el" "emacspeak-sql.el" "emacspeak-supercite.el"
;;;;;;  "emacspeak-tempo.el" "emacspeak-tide.el" "emacspeak-todo-mode.el"
;;;;;;  "emacspeak-typo.el" "emacspeak-wdired.el" "emacspeak-windmove.el"
;;;;;;  "emacspeak-winring.el" "emacspeak-woman.el" "emacspeak-xref.el"
;;;;;;  "emacspeak-yaml.el" "emacspeak-yasnippet.el") (0 0 0 0))

;;;***

;;;### (autoloads nil "acss-structure" "acss-structure.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from acss-structure.el

(register-definition-prefixes "acss-structure" '("acss-personality-from-speech-style" "tts-default-voice"))

;;;***
;;; cus-load.el --- automatically extracted custom dependencies
;;
;;; Code:

(put 'amixer 'custom-loads '(amixer))
(put 'applications 'custom-loads '(emacspeak soundscape sox xbacklight))
(put 'cd-tool 'custom-loads '(cd-tool))
(put 'dtk 'custom-loads '(dtk-speak dtk-unicode))
(put 'emacspeak 'custom-loads '(dtk-speak emacspeak emacspeak-add-log emacspeak-advice emacspeak-bookshare emacspeak-epub emacspeak-erc emacspeak-eshell emacspeak-eterm emacspeak-eww emacspeak-feeds emacspeak-flyspell emacspeak-gnus emacspeak-ido emacspeak-ispell emacspeak-keymap emacspeak-m-player emacspeak-message emacspeak-ocr emacspeak-pronounce emacspeak-sounds emacspeak-speak emacspeak-vm emacspeak-we emacspeak-wizards sox voice-setup xbacklight))
(put 'emacspeak-bookshare 'custom-loads '(emacspeak-bookshare))
(put 'emacspeak-epub 'custom-loads '(emacspeak-epub))
(put 'emacspeak-erc 'custom-loads '(emacspeak-erc))
(put 'emacspeak-feeds 'custom-loads '(emacspeak-feeds))
(put 'emacspeak-flyspell 'custom-loads '(emacspeak-flyspell))
(put 'emacspeak-gnus 'custom-loads '(emacspeak-gnus))
(put 'emacspeak-google 'custom-loads '(emacspeak-google))
(put 'emacspeak-ido 'custom-loads '(emacspeak-ido))
(put 'emacspeak-info 'custom-loads '(emacspeak-info))
(put 'emacspeak-ispell 'custom-loads '(emacspeak-ispell))
(put 'emacspeak-jabber 'custom-loads '(emacspeak-jabber))
(put 'emacspeak-librivox 'custom-loads '(emacspeak-librivox))
(put 'emacspeak-m-player 'custom-loads '(emacspeak-m-player))
(put 'emacspeak-media 'custom-loads '(emacspeak-m-player))
(put 'emacspeak-message 'custom-loads '(emacspeak-message))
(put 'emacspeak-ocr 'custom-loads '(emacspeak-ocr))
(put 'emacspeak-org 'custom-loads '(emacspeak-org))
(put 'emacspeak-speak 'custom-loads '(emacspeak-advice emacspeak-comint emacspeak-wizards))
(put 'emacspeak-url-template 'custom-loads '(emacspeak-url-template))
(put 'emacspeak-vm 'custom-loads '(emacspeak-vm))
(put 'emacspeak-we 'custom-loads '(emacspeak-we))
(put 'emacspeak-webspace 'custom-loads '(emacspeak-webspace))
(put 'emacspeak-wizards 'custom-loads '(emacspeak-wizards))
(put 'eshell 'custom-loads '(emacspeak-eshell))
(put 'flyspell 'custom-loads '(emacspeak-flyspell))
(put 'g 'custom-loads '(g-utils gmaps))
(put 'games 'custom-loads '(tetris))
(put 'gmaps 'custom-loads '(gmaps))
(put 'gnus 'custom-loads '(emacspeak-gnus))
(put 'gweb 'custom-loads '(gmaps))
(put 'message 'custom-loads '(emacspeak-message))
(put 'soundscape 'custom-loads '(soundscape))
(put 'sox 'custom-loads '(sox-gen))
(put 'tts 'custom-loads '(dectalk-voices dtk-speak espeak-voices mac-voices outloud-voices))
(put 'vm 'custom-loads '(emacspeak-vm))
(put 'voice-lock 'custom-loads '(voice-setup))

;; The remainder of this file is for handling :version.
;; We provide a minimum of information so that `customize-changed-options'
;; can do its job.

;; For groups we set `custom-version', `group-documentation' and
;; `custom-tag' (which are shown in the customize buffer), so we
;; don't have to load the file containing the group.

;; This macro is used so we don't modify the information about
;; variables and groups if it's already set. (We don't know when
;; cus-load.el is going to be loaded and at that time some of the
;; files might be loaded and some others might not).
(defmacro custom-put-if-not (symbol propname value)
  `(unless (get ,symbol ,propname)
     (put ,symbol ,propname ,value)))


(defvar custom-versions-load-alist nil
  "For internal use by custom.
This is an alist whose members have as car a version string, and as
elements the files that have variables or faces that contain that
version.  These files should be loaded before showing the customization
buffer that `customize-changed-options' generates.")


(provide 'cus-load)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; cus-load.el ends here
