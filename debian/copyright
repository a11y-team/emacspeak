Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: emacspeak
Upstream-Contact: T.V. Raman <raman@google.com>
Comment: The original source was repacked by removing the info directory as the
 emacspeak documentation has the following copyright and non-free license:
 .
 Permission is granted to make and distribute verbatim copies of this
 manual without charge provided the copyright notice and this permission
 notice are preserved on all copies.
 .
 Upstream also ships its cache with data blobs, strip that too.
Files-Excluded:
 info
 .ccls-cache

Files: *
Copyright: 1991-2011 Free Software Foundation
           1995-1996 William M. Perry <wmperry@aventail.com>
           1995-2018 T.V. Raman <raman@cs.cornell.edu>
           2001-2011  Dimitri V. Paduchih <paduch@imm.uran.ru>
           2007-2011 Lukas Loehrer
           Igor B. Poretsky
           1995-1996 William M. Perry <wmperry@aventail.com>
License: GPL-2+
Comment:
 From: "T. V. Raman" <raman@users.sf.net>
 To: jrvz@comcast.net
 Cc: tvraman@comcast.net, 292322@bugs.debian.org, jcasey@gnu.org
 Subject: emacspeak license
 Date: Sun, 30 Jan 2005 19:29:03 -0800
 The references to DEC and Adobe are legcy.
 .
 Each lisp file clearly states it's not part of Emacs but the same
 licensing rules apply --as far as I am concerned that's GPL.

Files: debian/*
Copyright: 2006-2010 James R. Van Zandt <jrv@debian.org>
           2013-2018 Paul Gevers <elbrus@debian.org>
License: GPL-2+

Files: etc/emacs-pipe.pl
Copyright: 2011 Mark Aufflick <mark@aufflick.com>
License: Expat

Files: etc/tips.html
       etc/tips.xml
Copyright: 2002 T.V. Raman
License: LDPL-2

Files: js/*
Copyright: 2016 T.V. Raman
           2016 Volker Sorge
License: Apache-2.0

Files: lisp/tapestry.el
Copyright: 1991-2011 Kyle E. Jones <kyle@uunet.uu.net>
License: GPL-1+

Files: lisp/tetris.el
Copyright: 1997-2011, Free Software Foundation, Inc.
License: GPL-3+

Files: lisp/toy-braille.el
Copyright: 2004 Marco Parrone <marc0@autistici.org>
License: dwyw
 This file is placed into the Public Domain.
 .
 The author puts no legal restrictions on what you can do with this
 file. So copying and distribution of this file, with or without
 modification, are permitted.

Files: servers/native-espeak/tclespeak.cpp
       servers/linux-outloud/atcleci.cpp
Copyright: 1999 Paige Phault
License: PP

Files: servers/linux-outloud/langswitch.h
Copyright: 2005-2006 IBM Corp.
License: BSD-3-clause

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
     http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS"BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License,
 Version 2.0 can be found in '/usr/share/common-licenses/Apache-2.0'.

License: BSD-3-clause
 The BSD License
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution. 
 .
   * Neither the name of foo nor the names of its
     contributors may be used to endorse or promote products derived from
     this software without specific prior written permission. 
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 The MIT License
 .
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall
 be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 1, February 1989, or (at
 your option) any later version
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 dated June, 2007, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 3 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-3'.

License: LDPL-2
  LINUX DOCUMENTATION PROJECT LICENSE (LDPL) v2.0, 12 January 1998
 .
 I COPYRIGHT
 .
 The copyright to each Linux Documentation Project (LDP) document is owned by
 its author or authors.
 .
 II LICENSE
 .
 The following license terms apply to all LDP documents, unless otherwise
 stated in the document. The LDP documents may be reproduced and distributed in
 whole or in part, in any medium physical or electronic, provided that this
 license notice is displayed in the reproduction. Commercial redistribution is
 permitted and encouraged. Thirty days advance notice via email to the
 author(s) of redistribution is appreciated, to give the authors time to
 provide updated documents.
 .
 A. REQUIREMENTS OF MODIFIED WORKS
 .
 All modified documents, including translations, anthologies, and partial
 documents, must meet the following requirements:
 .
 1 The modified version must be labeled as such.
 2 The person making the modifications must be identified.
 3 Acknowledgement of the original author must be retained.
 4 The location of the original unmodified document be identified.
 5 The original author's (or authors') name(s) may not be used to assert or
   imply endorsement of the resulting document without the original author's
   (or authors') permission.
 .
 In addition it is requested that:
 1 The modifications (including deletions) be noted.
 2 The author be notified by email of the modification in advance of
   redistribution, if an email address is provided in the document.
 .
 As a special exception, anthologies of LDP documents may include a single copy
 of these license terms in a conspicuous location within the anthology and
 replace other copies of this license with a reference to the single copy of
 the license without the document being considered "modified" for the purposes
 of this section.
 .
 Mere aggregation of LDP documents with other documents or programs on the same
 media shall not cause this license to apply to those other works.
 .
 All translations, derivative documents, or modified documents that incorporate
 any LDP document may not have more restrictive license terms than these,
 except that you may require distributors to make the resulting document
 available in source format.
 .
 LDP documents are available in source format via the LDP home page at
 http://sunsite.unc.edu/LDP/.

License: PP
 The author hereby grants permission to use, copy, modify, distribute, and
 license this software for any purpose, provided that existing copyright
 notices are retained in all copies and that this notice is included verbatim
 in any distributions. No written agreement, license, or royalty fee is
 required for any of the authorized uses. Modifications to this software may
 be copyrighted by their authors and need not follow the licensing terms
 described here, provided that the new terms are clearly indicated on the first
 page of each file where they apply.
 .
 IN NO EVENT SHALL THE AUTHORS OR DISTRIBUTORS BE LIABLE TO ANY PARTY FOR
 DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT OF
 THE USE OF THIS SOFTWARE, ITS DOCUMENTATION, OR ANY DERIVATIVES THEREOF, EVEN
 IF THE AUTHORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 THE AUTHORS AND DISTRIBUTORS SPECIFICALLY DISCLAIM ANY WARRANTIES, INCLUDING,
 BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 PARTICULAR PURPOSE, AND NON-INFRINGEMENT.  THIS SOFTWARE IS PROVIDED ON AN "AS
 IS" BASIS, AND THE AUTHORS AND DISTRIBUTORS HAVE NO OBLIGATION TO PROVIDE
 MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
